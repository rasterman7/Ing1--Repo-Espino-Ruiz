!classDefinition: #TerniLapilliTest category: #TerniLapilli!
TestCase subclass: #TerniLapilliTest
	instanceVariableNames: 'terniLapilli'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TerniLapilli'!

!TerniLapilliTest methodsFor: 'setup' stamp: 'FR 11/5/2018 20:12:25'!
setUp

	terniLapilli _ TerniLapilli new! !


!TerniLapilliTest methodsFor: 'tests' stamp: 'FR 11/5/2018 20:27:48'!
test01BoardIsEmptyAfterSetUp

	self assert: (terniLapilli isBoardEmpty) equals: true.! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'FR 11/5/2018 20:28:17'!
test02BoardIsNotEmptyAfterInsertion

	terniLapilli insertIn: 1@1.

	self assert: (terniLapilli isBoardEmpty) equals: false.! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'FR 11/8/2018 09:27:41'!
test03InsertingXTokenInAPositionMakesTokenInThatPositionX

	terniLapilli insertIn: 1@1.
	
	self assert: (terniLapilli isXTokenIn: 1@1).! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'FR 11/8/2018 09:28:10'!
test04NoTokenCanBeInsertedInAnInvalidPosition
	
	self 
        should: [ terniLapilli insertIn: 4@1 ]
        raise: Error
        withExceptionDo: [ :invalidTerniLapilli |
		self assert: TerniLapilli invalidPositionErrorMessage equals: invalidTerniLapilli messageText.
		self assert: (terniLapilli isNoTokenIn: 4@1)] ! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'FR 11/8/2018 09:30:01'!
test05InsertingOTokenInAPositionMakesTokenInThatPositionO

	terniLapilli insertIn: 1@2.
	terniLapilli insertIn: 1@1.
	
	self assert: (terniLapilli isOTokenIn: 1@1).! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'FR 11/8/2018 11:02:08'!
test07NoTokenCanBeInsertedInAnOccuppiedPosition

	terniLapilli insertIn: 1@1.

	self should: [terniLapilli insertIn: 1@1]
	raise: Error
	withExceptionDo: [:anInvalidTerniLapilli |
		self assert: TerniLapilli unavailablePositionErrorMessage equals: anInvalidTerniLapilli messageText.
		self assert: (terniLapilli isXTokenIn: 1@1)]! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'FR 11/8/2018 09:31:32'!
test10NoMoreTokensShouldBeInsertedAfterPlacing3OfEachKind

	(OrderedCollection with: 1@1 with: 1@2 with: 1@3 with: 2@1 with: 2@2 with: 2@3) do: [
		:aPosition | terniLapilli insertIn: aPosition ].

	self should: [terniLapilli insertIn: 3@1]
	raise: Error
	withExceptionDo: [:anInvalidTerniLapilli |
		self assert: TerniLapilli noMoreTokensShouldBeInsertedErrorMessage equals: anInvalidTerniLapilli messageText. 
		self assert: (terniLapilli isNoTokenIn: 3@1)]! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'FR 11/8/2018 09:32:05'!
test11SlidingXTokenFromAPositionToAnotherLeavesTheFormerEmptyAndTheLatterWithXToken

	(OrderedCollection with: 1@1 with: 1@2 with: 1@3 with: 2@1 with: 2@2 with: 2@3) do: [
		:aPosition | terniLapilli insertIn: aPosition ].
	
	terniLapilli slideFrom: 2@2 to: 3@2.

	self assert: (terniLapilli isNoTokenIn: 2@2).
	self assert: (terniLapilli isXTokenIn: 3@2).! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'IE 11/8/2018 14:16:41'!
test12NoTokenCanBeSlidToAnOpponentPosition

	(OrderedCollection with: 1@1 with: 1@2 with: 1@3 with: 2@1 with: 2@2 with: 2@3) do: [
		:aPosition | terniLapilli insertIn: aPosition ].
	
	self should: [terniLapilli slideFrom: 2@1 to: 3@1]
	raise: Error
	withExceptionDo: [:invalidTerniLapilli |
		self assert: TerniLapilli cantSlideFromOpponentTokenErrorMessage equals: invalidTerniLapilli messageText.
		self assert: (terniLapilli isOTokenIn: 2@1).
		self assert: (terniLapilli isNoTokenIn: 3@1)]! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'FR 11/8/2018 09:33:15'!
test13SlidingOTokenFromAPositionToAnotherLeavesTheFormerEmptyAndTheLatterWithOToken

	(OrderedCollection with: 1@1 with: 1@2 with: 1@3 with: 2@1 with: 2@2 with: 2@3) do: [
		:aPosition | terniLapilli insertIn: aPosition ].
	
	terniLapilli slideFrom: 2@2 to: 3@2.
	terniLapilli slideFrom: 2@3 to: 3@3.

	self assert: (terniLapilli isNoTokenIn: 2@3).
	self assert: (terniLapilli isOTokenIn: 3@3).! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'FR 11/8/2018 09:33:34'!
test14NoTokenCanBeSlidToAnInvalidPosition

	(OrderedCollection with: 1@1 with: 1@2 with: 1@3 with: 2@1 with: 2@2 with: 2@3) do: [
		:aPosition | terniLapilli insertIn: aPosition ].
	
	self should: [terniLapilli slideFrom: 4@4 to: 3@1]
	raise: Error
	withExceptionDo: [:invalidTerniLapilli |
		self assert: TerniLapilli invalidPositionErrorMessage equals: invalidTerniLapilli messageText.
		self assert: (terniLapilli isNoTokenIn: 3@1)]! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'FR 11/8/2018 09:34:10'!
test15NoTokenCanBeSlidFromEmptyPosition

	(OrderedCollection with: 1@1 with: 1@2 with: 1@3 with: 2@1 with: 2@2 with: 2@3) do: [
		:aPosition | terniLapilli insertIn: aPosition ].
	
	self should: [terniLapilli slideFrom: 3@1 to: 3@2]
	raise: Error
	withExceptionDo: [:invalidTerniLapilli |
		self assert: TerniLapilli noTokenToSlideInPositionErrorMessage equals: invalidTerniLapilli messageText.
		self assert: (terniLapilli isNoTokenIn: 3@1).
		self assert: (terniLapilli isNoTokenIn: 3@2)]! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'FR 11/8/2018 11:07:45'!
test16TokensCanOnlyBeSlidToAdjacentPositions

	(OrderedCollection with: 1@1 with: 1@2 with: 1@3 with: 2@1 with: 3@1 with: 2@3) do: [
		:aPosition | terniLapilli insertIn: aPosition ].
	
	self should: [terniLapilli slideFrom: 3@1 to: 3@3]
	raise: Error
	withExceptionDo: [:invalidTerniLapilli |
		self assert: TerniLapilli cantSlideMoreThanOnePositionErrorMessage equals: invalidTerniLapilli messageText.
		self assert: (terniLapilli isXTokenIn: 3@1).
		self assert: (terniLapilli isNoTokenIn: 3@3)]! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'FR 11/8/2018 09:34:40'!
test16TokensCanOnlyBeSlidToAnAdjacentPosition

	(OrderedCollection with: 1@1 with: 1@2 with: 1@3 with: 2@1 with: 3@1 with: 2@3) do: [
		:aPosition | terniLapilli insertIn: aPosition ].
	
	self should: [terniLapilli slideFrom: 3@1 to: 3@3]
	raise: Error
	withExceptionDo: [:invalidTerniLapilli |
		self assert: TerniLapilli cantSlideMoreThanOnePositionErrorMessage equals: invalidTerniLapilli messageText.
		self assert: (terniLapilli isXTokenIn: 3@1).
		self assert: (terniLapilli isNoTokenIn: 3@3)]! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'FR 11/6/2018 17:37:38'!
test17GameIsOverWhenAPlayerFillsTheFirstRow

	(OrderedCollection with: 1@1 with: 2@2 with: 1@3 with: 2@1 with: 1@2) do: [
		:aPosition | terniLapilli insertIn: aPosition ].
	
	self assert: terniLapilli isGameOver! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'FR 11/6/2018 17:40:22'!
test18GameIsNotOverWhenNoPlayerHasAlligned3Tokens

	(OrderedCollection with: 1@1 with: 2@2 with: 1@3 with: 2@1 with: 3@2) do: [
		:aPosition | terniLapilli insertIn: aPosition ].
	
	self deny: terniLapilli isGameOver! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'FR 11/6/2018 17:40:46'!
test19GameIsOverWhenAPlayerFillsTheSecondRow

	(OrderedCollection with: 2@1 with: 1@2 with: 2@3 with: 1@1 with: 2@2) do: [
		:aPosition | terniLapilli insertIn: aPosition ].
	
	self assert: terniLapilli isGameOver! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'FR 11/6/2018 17:41:00'!
test20GameIsOverWhenAPlayerFillsTheThridRow

	(OrderedCollection with: 3@1 with: 1@2 with: 3@3 with: 1@1 with: 3@2) do: [
		:aPosition | terniLapilli insertIn: aPosition ].
	
	self assert: terniLapilli isGameOver! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'FR 11/6/2018 17:41:11'!
test21GameIsOverWhenAPlayerFillsTheFirstColumn

	(OrderedCollection with: 1@1 with: 1@2 with: 2@1 with: 1@3 with: 3@1) do: [
		:aPosition | terniLapilli insertIn: aPosition ].
	
	self assert: terniLapilli isGameOver! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'FR 11/6/2018 17:41:30'!
test22GameIsOverWhenAPlayerFillsTheSecondColumn

	(OrderedCollection with: 1@2 with: 1@1 with: 2@2 with: 1@3 with: 3@2) do: [
		:aPosition | terniLapilli insertIn: aPosition ].
	
	self assert: terniLapilli isGameOver! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'FR 11/6/2018 17:41:44'!
test23GameIsOverWhenAPlayerFillsTheThirdColumn

	(OrderedCollection with: 1@3 with: 1@1 with: 2@3 with: 1@2 with: 3@3) do: [
		:aPosition | terniLapilli insertIn: aPosition ].

	self assert: terniLapilli isGameOver! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'FR 11/6/2018 17:43:22'!
test24GameIsOverWhenAPlayerFillsTheUpLeftToDownRightDiagonal

	(OrderedCollection with: 1@1 with: 1@2 with: 2@2 with: 2@1 with: 3@3) do: [
		:aPosition | terniLapilli insertIn: aPosition ].
	
	self assert: terniLapilli isGameOver! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'FR 11/6/2018 17:43:49'!
test25GameIsOverWhenAPlayerFillsTheDownLeftToUpRightDiagonal

	(OrderedCollection with: 3@1 with: 1@2 with: 2@2 with: 2@1 with: 1@3) do: [
		:aPosition | terniLapilli insertIn: aPosition ].
	
	self assert: terniLapilli isGameOver! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'FR 11/8/2018 09:34:56'!
test26NoTokenShouldBeInsertedAfterGameIsOver

	(OrderedCollection with: 3@1 with: 1@2 with: 2@2 with: 2@1 with: 1@3) do: [
		:aPosition | terniLapilli insertIn: aPosition ].
	
	self should: [terniLapilli insertIn: 3@2]
	raise: Error
	withExceptionDo: [:invalidTerniLapilli |
		self assert: TerniLapilli cantInsertTokenWhenGameIsOverErrorMessage equals: invalidTerniLapilli messageText.
		self assert: (terniLapilli isNoTokenIn: 3@2)]! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'FR 11/8/2018 09:36:55'!
test27NoTokenShouldBeSlidAfterGameIsOver

	(OrderedCollection with: 3@1 with: 1@2 with: 2@1 with: 2@2 with: 1@3 with: 3@2) do: [
		:aPosition | terniLapilli insertIn: aPosition ].
	
	self should: [terniLapilli slideFrom: 1@3 to: 2@3]
	raise: Error
	withExceptionDo: [:invalidTerniLapilli |
		self assert: TerniLapilli cantSlideTokenWhenGameIsOverErrorMessage equals: invalidTerniLapilli messageText.
		self assert: (terniLapilli isXTokenIn: 1@3).
		self assert: (terniLapilli isNoTokenIn: 2@3)]! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'FR 11/6/2018 17:47:31'!
test29XPlayerWinsAfterAlligningTheirTokens

	(OrderedCollection with: 3@1 with: 1@3 with: 2@1 with: 2@2 with: 1@2 with: 3@2) do: [
		:aPosition | terniLapilli insertIn: aPosition ].
	terniLapilli slideFrom: 1@2 to: 1@1.
	
	self assert: terniLapilli isGameOver.
	self assert: terniLapilli hasXWon.
	self deny: terniLapilli hasOWon.! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'FR 11/6/2018 17:47:26'!
test30OPlayerWinsAfterAlliginingTheirTokens

	(OrderedCollection with: 3@1 with: 1@2 with: 2@1 with: 2@2 with: 1@3 with: 3@2) do: [
		:aPosition | terniLapilli insertIn: aPosition ].
	
	self assert: terniLapilli isGameOver.
	self assert: terniLapilli hasOWon.
	self deny: terniLapilli hasXWon! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'FR 11/6/2018 17:58:59'!
test31XPlayerStartsTheGame
	
	self assert: terniLapilli isCurrentPlayerX! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'FR 11/6/2018 18:15:15'!
test32OPlayerFollowsXPlayer

	terniLapilli insertIn: 1@1. 
	
	self deny: terniLapilli isCurrentPlayerX! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'FR 11/8/2018 09:37:22'!
test33DiagonalSlidsShouldInvolveTheCenterPosition

	(OrderedCollection with: 1@1 with: 1@2 with: 2@1 with: 2@2 with: 3@3 with: 3@1) do: [
		:aPosition | terniLapilli insertIn: aPosition ].
	
	self should: [terniLapilli slideFrom: 2@1 to: 3@2]
	raise: Error
	withExceptionDo: [:invalidTerniLapilli |
		self assert: TerniLapilli cantSlideMoreThanOnePositionErrorMessage equals: invalidTerniLapilli messageText.
		self assert: (terniLapilli isNoTokenIn: 3@2).
		self assert: (terniLapilli isXTokenIn: 2@1)]! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'FR 11/8/2018 10:22:30'!
test34NoTokenCanBeSlidBeforeAllHaveBeenPlaced

	(OrderedCollection with: 1@1 with: 1@2 with: 2@2 with: 2@1 with: 1@3) do: [
		:aPosition | terniLapilli insertIn: aPosition ].
	
	self should: [terniLapilli slideFrom: 3@2 to: 3@3]
	raise: Error
	withExceptionDo: [:invalidTerniLapilli |
		self assert: TerniLapilli cantSlideBeforeAllTokensHaveBeenPlacedErrorMessage equals: invalidTerniLapilli messageText.
		self assert: (terniLapilli isNoTokenIn: 3@2)]! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'FR 11/8/2018 11:09:52'!
test35NoTokenCanBeSlidToAnAlreadyOccuppiedPosition

	(OrderedCollection with: 1@1 with: 1@2 with: 1@3 with: 2@1 with: 2@2 with: 2@3) do: [
		:aPosition | terniLapilli insertIn: aPosition ].
	
	self should: [terniLapilli slideFrom: 2@2 to: 1@3]
	raise: Error
	withExceptionDo: [:invalidTerniLapilli |
		self assert: TerniLapilli unavailablePositionErrorMessage equals: invalidTerniLapilli messageText.
		self assert: (terniLapilli isOTokenIn: 2@1).
		self assert: (terniLapilli isNoTokenIn: 3@1)]! !


!classDefinition: #TerniLapilli category: #TerniLapilli!
Object subclass: #TerniLapilli
	instanceVariableNames: 'board nextPlayer players state'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TerniLapilli'!

!TerniLapilli methodsFor: 'testing' stamp: 'IE 11/8/2018 14:16:29'!
checkIfToken: aToken canBeSlidFrom: aFromPosition to: aToPosition
	
	((self isPositionValid: aFromPosition) and: [self isPositionValid: aToPosition]) ifFalse: [self error: self class invalidPositionErrorMessage].
	(self isNoTokenIn: aFromPosition) ifTrue: [self error: self class noTokenToSlideInPositionErrorMessage].
	(self is: aFromPosition neighborOf: aToPosition) ifFalse: [self error: self class cantSlideMoreThanOnePositionErrorMessage].
	(board includesKey: aToPosition) ifTrue: [self error: self class unavailablePositionErrorMessage].
	(self isToken: aToken in: aFromPosition) ifFalse: [self error: self class cantSlideFromOpponentTokenErrorMessage].! !

!TerniLapilli methodsFor: 'testing' stamp: 'FR 11/8/2018 10:39:11'!
is: aFromPosition neighborOf: aToPosition

	^ (aFromPosition eightNeighbors includes: aToPosition) and: [((2@2) fourNeighbors includes: aFromPosition) not or: [aFromPosition fourNeighbors includes: aToPosition]]! !

!TerniLapilli methodsFor: 'testing' stamp: 'FR 11/5/2018 19:48:57'!
isBoardEmpty
	
	^ board isEmpty
	! !

!TerniLapilli methodsFor: 'testing' stamp: 'FR 11/6/2018 17:57:07'!
isCurrentPlayerX
	
	^ state isCurrentPlayerX! !

!TerniLapilli methodsFor: 'testing' stamp: 'FR 11/8/2018 08:44:07'!
isNoTokenIn: aPosition
	
	^ ((self isXTokenIn: aPosition) or: [self isOTokenIn: aPosition]) not! !

!TerniLapilli methodsFor: 'testing' stamp: 'FR 11/8/2018 11:45:44'!
isOTokenIn: aPosition
	
	^ self isToken: 'O' in: aPosition! !

!TerniLapilli methodsFor: 'testing' stamp: 'FR 11/8/2018 10:08:54'!
isPositionValid: aPosition 
	
	^ aPosition = (2@2) or: [(2@2) eightNeighbors includes: aPosition]! !

!TerniLapilli methodsFor: 'testing' stamp: 'FR 11/8/2018 11:30:06'!
isToken: aToken in: aPosition
	
	^ (self tokenIn: aPosition ifAbsent: [false]) = aToken! !

!TerniLapilli methodsFor: 'testing' stamp: 'FR 11/8/2018 11:45:35'!
isXTokenIn: aPosition
	
	^ self isToken: 'X' in: aPosition! !

!TerniLapilli methodsFor: 'testing' stamp: 'FR 11/5/2018 19:01:16'!
tokenIn: aPosition ifAbsent: aBlock
	
	^ board at: aPosition ifAbsent: aBlock! !


!TerniLapilli methodsFor: 'placing' stamp: 'FR 11/5/2018 21:40:54'!
insertAfterAllTokensHaveBeenPlaced
	
	self error: self class noMoreTokensShouldBeInsertedErrorMessage! !

!TerniLapilli methodsFor: 'placing' stamp: 'FR 11/5/2018 21:41:32'!
insertAfterGameIsOver
	
	self error: self class cantInsertTokenWhenGameIsOverErrorMessage! !

!TerniLapilli methodsFor: 'placing' stamp: 'FR 11/5/2018 21:41:46'!
insertIn: aPosition 
	
	state insertIn: aPosition! !

!TerniLapilli methodsFor: 'placing' stamp: 'FR 11/8/2018 10:09:54'!
insertOTokenIn: aPosition 
	
	(self isPositionValid: aPosition) ifFalse: [self error: self class invalidPositionErrorMessage]. 
	(board includesKey: aPosition) ifTrue: [self error: self class unavailablePositionErrorMessage].

	board at: aPosition put: 'O'.
	(self isWinner: 'O') ifTrue: [state _ OWinnerState for: self] ifFalse: [
		(board occurrencesOf: 'O') = 3 ifTrue: [state _ XSlideState for: self] ifFalse: [state _ XPlacementState for: self]].! !

!TerniLapilli methodsFor: 'placing' stamp: 'FR 11/8/2018 10:13:21'!
insertXTokenIn: aPosition 
	
	(self isPositionValid: aPosition) ifFalse: [self error: self class invalidPositionErrorMessage]. 
	(board includesKey: aPosition) ifTrue: [self error: self class unavailablePositionErrorMessage].

	board at: aPosition put: 'X'.
	(self isWinner: 'X') ifTrue: [state _ XWinnerState for: self] ifFalse: [state _ OPlacementState for: self].! !


!TerniLapilli methodsFor: 'sliding' stamp: 'FR 11/5/2018 21:53:49'!
slideAfterGameIsOver
	
	self error: self class cantSlideTokenWhenGameIsOverErrorMessage! !

!TerniLapilli methodsFor: 'sliding' stamp: 'FR 11/5/2018 21:52:49'!
slideBeforeAllTokensHaveBeenPlaced
	
	self error: self class cantSlideBeforeAllTokensHaveBeenPlacedErrorMessage! !

!TerniLapilli methodsFor: 'sliding' stamp: 'FR 11/6/2018 18:04:37'!
slideFrom: aFromPosition to: aToPosition
	
	state slideFrom: aFromPosition to: aToPosition! !

!TerniLapilli methodsFor: 'sliding' stamp: 'FR 11/8/2018 11:28:06'!
slideOTokenFrom: aFromPosition to: aToPosition
	
	self checkIfToken: 'O' canBeSlidFrom: aFromPosition to: aToPosition.
	
	board removeKey: aFromPosition.

	board at: aToPosition put: 'O'.
	(self isWinner: 'O') ifTrue: [state _ OWinnerState for: self] ifFalse: [state _ XSlideState for: self].! !

!TerniLapilli methodsFor: 'sliding' stamp: 'FR 11/8/2018 11:28:06'!
slideXTokenFrom: aFromPosition to: aToPosition
	
	self checkIfToken: 'X' canBeSlidFrom: aFromPosition to: aToPosition.
	
	board removeKey: aFromPosition.

	board at: aToPosition put: 'X'.
	(self isWinner: 'X') ifTrue: [state _ XWinnerState for: self] ifFalse: [state _ OSlideState for: self].! !


!TerniLapilli methodsFor: 'initialization' stamp: 'FR 11/6/2018 17:50:27'!
initialize

	board _ Dictionary new.
	state _ XPlacementState for: self.! !


!TerniLapilli methodsFor: 'game over testing' stamp: 'FR 11/8/2018 11:39:54'!
has: aToken wonByColumn: aColumnNumber

	^#(1 2 3) allSatisfy: [:aRowNumber | self isToken: aToken in: aRowNumber@aColumnNumber]! !

!TerniLapilli methodsFor: 'game over testing' stamp: 'FR 11/8/2018 11:40:11'!
has: aToken wonByRow: aRowNumber

	^#(1 2 3) allSatisfy: [:aColumnNumber | self isToken: aToken in: aRowNumber@aColumnNumber]! !

!TerniLapilli methodsFor: 'game over testing' stamp: 'FR 11/8/2018 09:55:39'!
hasOWon
	
	^ state hasOWon! !

!TerniLapilli methodsFor: 'game over testing' stamp: 'FR 11/8/2018 11:22:06'!
hasXWon
	
	^ state hasXWon! !

!TerniLapilli methodsFor: 'game over testing' stamp: 'FR 11/8/2018 11:18:07'!
isGameOver
	
	^ state hasXWon or: [state hasOWon]! !

!TerniLapilli methodsFor: 'game over testing' stamp: 'FR 11/5/2018 17:42:37'!
isWinner: aToken
	
	| hasTokenWon |
	
	hasTokenWon _ self isWinnerByColumn: aToken.
	hasTokenWon _ hasTokenWon or: [self isWinnerByRow: aToken].
	hasTokenWon _ hasTokenWon or: [self isWinnerByUpLeftToDownRightDiagonal: aToken].
	hasTokenWon _ hasTokenWon or: [self isWinnerByDownLeftToUpRightDiagonal: aToken].
	
	^hasTokenWon! !

!TerniLapilli methodsFor: 'game over testing' stamp: 'FR 11/5/2018 17:30:56'!
isWinnerByColumn: aToken

	^#(1 2 3) anySatisfy: [:aColumnNumber | self has: aToken wonByColumn: aColumnNumber ].! !

!TerniLapilli methodsFor: 'game over testing' stamp: 'FR 11/8/2018 11:39:26'!
isWinnerByDownLeftToUpRightDiagonal: aToken

	^#(1 2 3) allSatisfy: [:aCoordinateNumber | self isToken: aToken in: (4-aCoordinateNumber)@aCoordinateNumber]! !

!TerniLapilli methodsFor: 'game over testing' stamp: 'FR 11/5/2018 17:36:12'!
isWinnerByRow: aToken

	^#(1 2 3) anySatisfy: [:aRowNumber | self has: aToken wonByRow: aRowNumber ].! !

!TerniLapilli methodsFor: 'game over testing' stamp: 'FR 11/8/2018 11:43:46'!
isWinnerByUpLeftToDownRightDiagonal: aToken

	^#(1 2 3) allSatisfy: [:aCoordinateNumber | self isToken: aToken in: aCoordinateNumber@aCoordinateNumber]! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'TerniLapilli class' category: #TerniLapilli!
TerniLapilli class
	instanceVariableNames: ''!

!TerniLapilli class methodsFor: 'error messages' stamp: 'FR 11/3/2018 16:39:57'!
cantInsertTokenWhenGameIsOverErrorMessage
	
	^'A token cant be inserted after the game is over'! !

!TerniLapilli class methodsFor: 'error messages' stamp: 'FR 11/8/2018 10:21:19'!
cantSlideBeforeAllTokensHaveBeenPlacedErrorMessage
	
	^'No token can de slide before all of them have been placed'! !

!TerniLapilli class methodsFor: 'error messages' stamp: 'IE 11/8/2018 14:16:08'!
cantSlideFromOpponentTokenErrorMessage
	
	^'Cannot slide from position with opponent token'! !

!TerniLapilli class methodsFor: 'error messages' stamp: 'FR 11/2/2018 18:14:20'!
cantSlideMoreThanOnePositionErrorMessage
	
	^'Cannot slide token more than one position'! !

!TerniLapilli class methodsFor: 'error messages' stamp: 'FR 11/4/2018 13:22:40'!
cantSlideTokenWhenGameIsOverErrorMessage
	
	^'A token cannot be slid after the game is over'! !

!TerniLapilli class methodsFor: 'error messages' stamp: 'FR 11/1/2018 19:55:35'!
invalidPositionErrorMessage
	
	^'Position is invalid'! !

!TerniLapilli class methodsFor: 'error messages' stamp: 'FR 11/1/2018 20:55:22'!
itsNotYourTurnErrorMessage
	
	^'Its not your turn'! !

!TerniLapilli class methodsFor: 'error messages' stamp: 'FR 11/2/2018 14:27:09'!
noMoreTokensShouldBeInsertedErrorMessage
	
	^'No more tokens should be inserted'! !

!TerniLapilli class methodsFor: 'error messages' stamp: 'FR 11/2/2018 16:30:27'!
noTokenToSlideInPositionErrorMessage
	
	^'No token in position to slide'! !

!TerniLapilli class methodsFor: 'error messages' stamp: 'FR 11/1/2018 20:43:51'!
playerXShouldGoFirstErrorMessage
	
	^'Player X should go first'! !

!TerniLapilli class methodsFor: 'error messages' stamp: 'FR 11/1/2018 20:27:14'!
unavailablePositionErrorMessage
	
	^'Position is unavailable'! !


!classDefinition: #TerniLapilliState category: #TerniLapilli!
Object subclass: #TerniLapilliState
	instanceVariableNames: 'game'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TerniLapilli'!

!TerniLapilliState methodsFor: 'testing' stamp: 'FR 11/8/2018 10:03:13'!
hasOWon

	^ false! !

!TerniLapilliState methodsFor: 'testing' stamp: 'FR 11/8/2018 10:04:03'!
hasXWon

	^ false! !

!TerniLapilliState methodsFor: 'testing' stamp: 'FR 11/6/2018 17:57:07'!
isCurrentPlayerX

	self subclassResponsibility ! !


!TerniLapilliState methodsFor: 'initialization' stamp: 'FR 11/5/2018 21:24:20'!
initializeFor: aTerniLapilliGame

	game _ aTerniLapilliGame ! !


!TerniLapilliState methodsFor: 'placing' stamp: 'FR 11/5/2018 21:27:39'!
insertIn: aPosition

	self subclassResponsibility ! !


!TerniLapilliState methodsFor: 'sliding' stamp: 'FR 11/5/2018 21:43:58'!
slideFrom: aFromPosition to: aToPosition

	self subclassResponsibility ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'TerniLapilliState class' category: #TerniLapilli!
TerniLapilliState class
	instanceVariableNames: ''!

!TerniLapilliState class methodsFor: 'instance creation' stamp: 'FR 11/5/2018 21:55:22'!
for: aTerniLapilliGame

	^self new initializeFor: aTerniLapilliGame ! !


!classDefinition: #OPlacementState category: #TerniLapilli!
TerniLapilliState subclass: #OPlacementState
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TerniLapilli'!

!OPlacementState methodsFor: 'placing' stamp: 'FR 11/5/2018 21:29:28'!
insertIn: aPosition

	game insertOTokenIn: aPosition ! !


!OPlacementState methodsFor: 'testing' stamp: 'FR 11/6/2018 17:57:07'!
isCurrentPlayerX

	^false! !


!OPlacementState methodsFor: 'sliding' stamp: 'FR 11/5/2018 21:44:31'!
slideFrom: aFromPosition to: aToPosition

	game slideBeforeAllTokensHaveBeenPlaced! !


!classDefinition: #OSlideState category: #TerniLapilli!
TerniLapilliState subclass: #OSlideState
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TerniLapilli'!

!OSlideState methodsFor: 'placing' stamp: 'FR 11/5/2018 21:31:02'!
insertIn: aPosition

	game insertAfterAllTokensHaveBeenPlaced! !


!OSlideState methodsFor: 'testing' stamp: 'FR 11/6/2018 17:57:07'!
isCurrentPlayerX

	^false! !


!OSlideState methodsFor: 'sliding' stamp: 'FR 11/5/2018 21:44:38'!
slideFrom: aFromPosition to: aToPosition

	game slideOTokenFrom: aFromPosition to: aToPosition ! !


!classDefinition: #OWinnerState category: #TerniLapilli!
TerniLapilliState subclass: #OWinnerState
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TerniLapilli'!

!OWinnerState methodsFor: 'testing' stamp: 'FR 11/8/2018 09:57:50'!
hasOWon
	
	^ true! !

!OWinnerState methodsFor: 'testing' stamp: 'FR 11/6/2018 17:57:07'!
isCurrentPlayerX

	^false! !


!OWinnerState methodsFor: 'placing' stamp: 'FR 11/5/2018 21:37:33'!
insertIn: aPosition

	game insertAfterGameIsOver! !


!OWinnerState methodsFor: 'sliding' stamp: 'FR 11/5/2018 21:44:55'!
slideFrom: aFromPosition to: aToPosition

	game slideAfterGameIsOver! !


!classDefinition: #XPlacementState category: #TerniLapilli!
TerniLapilliState subclass: #XPlacementState
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TerniLapilli'!

!XPlacementState methodsFor: 'placing' stamp: 'FR 11/5/2018 21:29:45'!
insertIn: aPosition

	game insertXTokenIn: aPosition ! !


!XPlacementState methodsFor: 'testing' stamp: 'FR 11/6/2018 17:57:07'!
isCurrentPlayerX

	^true! !


!XPlacementState methodsFor: 'sliding' stamp: 'FR 11/5/2018 21:45:17'!
slideFrom: aFromPosition to: aToPosition

	game slideBeforeAllTokensHaveBeenPlaced! !


!classDefinition: #XSlideState category: #TerniLapilli!
TerniLapilliState subclass: #XSlideState
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TerniLapilli'!

!XSlideState methodsFor: 'placing' stamp: 'FR 11/5/2018 21:31:20'!
insertIn: aPosition

	game insertAfterAllTokensHaveBeenPlaced! !


!XSlideState methodsFor: 'testing' stamp: 'FR 11/6/2018 17:57:07'!
isCurrentPlayerX

	^true! !


!XSlideState methodsFor: 'sliding' stamp: 'FR 11/5/2018 21:45:23'!
slideFrom: aFromPosition to: aToPosition

	game slideXTokenFrom: aFromPosition to: aToPosition ! !


!classDefinition: #XWinnerState category: #TerniLapilli!
TerniLapilliState subclass: #XWinnerState
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TerniLapilli'!

!XWinnerState methodsFor: 'testing' stamp: 'FR 11/8/2018 09:58:36'!
hasXWon
	
	^ true! !

!XWinnerState methodsFor: 'testing' stamp: 'FR 11/6/2018 17:57:07'!
isCurrentPlayerX

	^true! !


!XWinnerState methodsFor: 'placing' stamp: 'FR 11/5/2018 21:37:39'!
insertIn: aPosition

	game insertAfterGameIsOver! !


!XWinnerState methodsFor: 'sliding' stamp: 'FR 11/5/2018 21:45:36'!
slideFrom: aFromPosition to: aToPosition

	game slideAfterGameIsOver! !
