!classDefinition: #QueueTest category: #'Queue-Exercise'!
TestCase subclass: #QueueTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Queue-Exercise'!

!QueueTest methodsFor: 'test' stamp: 'HAW 9/26/2018 23:15:51'!
test01QueueShouldBeEmptyWhenCreated

	| queue |

	queue _ Queue new.

	self assert: queue isEmpty.! !

!QueueTest methodsFor: 'test' stamp: 'HAW 9/26/2018 23:15:51'!
test02EnqueueAddElementsToTheQueue

	| queue |

	queue _ Queue new.
	queue enqueue: 'Something'.

	self deny: queue isEmpty.! !

!QueueTest methodsFor: 'test' stamp: 'HAW 9/26/2018 23:15:51'!
test03DequeueRemovesElementsFromTheQueue

	| queue |

	queue _ Queue new.
	queue enqueue: 'Something'.
	queue dequeue.

	self assert: queue isEmpty.! !

!QueueTest methodsFor: 'test' stamp: 'HAW 9/26/2018 23:15:51'!
test04DequeueReturnsFirstEnqueuedObject

	| queue firstQueued secondQueued |

	queue _ Queue new.
	firstQueued _ 'Something1'.
	secondQueued _ 'Something2'.
	queue enqueue: firstQueued.
	queue enqueue: secondQueued.
	
	self assert: queue dequeue equals: firstQueued.! !

!QueueTest methodsFor: 'test' stamp: 'CGCM 9/27/2018 16:34:58'!
test05QueueBehavesFIFO

	| queue firstQueued secondQueued |

	queue _ Queue new.
	firstQueued _ 'First'.
	secondQueued _ 'Second'.
	queue enqueue: firstQueued.
	queue enqueue: secondQueued.

	self assert: queue dequeue equals: firstQueued.
	self assert: queue dequeue equals: secondQueued.
	self assert: queue isEmpty.! !

!QueueTest methodsFor: 'test' stamp: 'HAW 9/26/2018 23:15:51'!
test06NextReturnsFirstEnqueuedObject

	| queue firstQueued |

	queue _ Queue new.
	firstQueued _ 'Something'.
	queue enqueue: firstQueued.

	self assert: queue next equals: firstQueued.! !

!QueueTest methodsFor: 'test' stamp: 'IE 9/29/2018 02:20:48'!
test07NextDoesNotRemoveObjectFromQueue

	| queue firstQueued |

	queue _ Queue new.
	firstQueued _ 'Something'.
	queue enqueue: firstQueued.

	queue next.

	self assert: queue size equals: 1.! !

!QueueTest methodsFor: 'test' stamp: 'HAW 9/26/2018 23:15:51'!
test08CanNotDequeueWhenThereAreNoObjectsInTheQueue

	| queue |

	queue _ Queue new.
	
	self
		should: [ queue dequeue ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText equals: Queue queueEmptyErrorDescription ].! !

!QueueTest methodsFor: 'test' stamp: 'HAW 9/26/2018 23:15:51'!
test09CanNotDequeueWhenThereAreNoObjectsInTheQueueAndTheQueueHadObjects

	| queue |

	queue _ Queue new.
	queue enqueue: 'Something'.
	queue dequeue.

	self
		should: [ queue dequeue ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText equals: Queue queueEmptyErrorDescription ].! !

!QueueTest methodsFor: 'test' stamp: 'HAW 9/26/2018 23:15:51'!
test10CanNotNextWhenThereAreNoObjectsInTheQueue

	| queue |

	queue _ Queue new.

	self
		should: [ queue next ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText equals: Queue queueEmptyErrorDescription ].! !


!classDefinition: #DequeueAndNextHandler category: #'Queue-Exercise'!
Object subclass: #DequeueAndNextHandler
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Queue-Exercise'!

!DequeueAndNextHandler methodsFor: 'operations' stamp: 'FR 10/3/2018 14:04:59'!
dequeue: aCollection
	
	self subclassResponsibility! !

!DequeueAndNextHandler methodsFor: 'operations' stamp: 'FR 10/3/2018 14:05:06'!
next: aCollection

	self subclassResponsibility ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'DequeueAndNextHandler class' category: #'Queue-Exercise'!
DequeueAndNextHandler class
	instanceVariableNames: ''!

!DequeueAndNextHandler class methodsFor: 'Handler search' stamp: 'IE 10/3/2018 11:27:12'!
for: aQueue

	| aQueueHandlerSubclass |
	
	aQueueHandlerSubclass _ subclasses detect: [:aQueueHandler | aQueueHandler isFor: aQueue].
	
	^ aQueueHandlerSubclass new! !

!DequeueAndNextHandler class methodsFor: 'Handler search' stamp: 'IE 10/3/2018 11:21:28'!
isFor: aQueue

	self subclassResponsibility.! !


!classDefinition: #EmptyQueueHandler category: #'Queue-Exercise'!
DequeueAndNextHandler subclass: #EmptyQueueHandler
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Queue-Exercise'!

!EmptyQueueHandler methodsFor: 'operations' stamp: 'FR 10/3/2018 14:04:37'!
dequeue: anEmptyCollection

	^Queue error: Queue queueEmptyErrorDescription! !

!EmptyQueueHandler methodsFor: 'operations' stamp: 'FR 10/3/2018 14:04:52'!
next: anEmptyCollection

	^Queue error: Queue queueEmptyErrorDescription.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'EmptyQueueHandler class' category: #'Queue-Exercise'!
EmptyQueueHandler class
	instanceVariableNames: ''!

!EmptyQueueHandler class methodsFor: 'Handler search' stamp: 'IE 10/3/2018 09:41:22'!
isFor: aQueue

	^aQueue isEmpty.

	! !


!classDefinition: #FilledQueueHandler category: #'Queue-Exercise'!
DequeueAndNextHandler subclass: #FilledQueueHandler
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Queue-Exercise'!

!FilledQueueHandler methodsFor: 'operations' stamp: 'FR 10/3/2018 14:06:59'!
dequeue: anOrderedCollection
	
	^anOrderedCollection removeFirst! !

!FilledQueueHandler methodsFor: 'operations' stamp: 'FR 10/3/2018 14:07:15'!
next: anOrderedCollection

	^anOrderedCollection first! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'FilledQueueHandler class' category: #'Queue-Exercise'!
FilledQueueHandler class
	instanceVariableNames: ''!

!FilledQueueHandler class methodsFor: 'Handler search' stamp: 'IE 10/3/2018 11:15:30'!
isFor: aQueue

	^aQueue isEmpty = false.! !


!classDefinition: #Queue category: #'Queue-Exercise'!
Object subclass: #Queue
	instanceVariableNames: 'contents'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Queue-Exercise'!

!Queue methodsFor: 'initialization' stamp: 'FR 10/3/2018 14:01:43'!
initialize

	contents := OrderedCollection new.! !


!Queue methodsFor: 'operations' stamp: 'IE 10/4/2018 16:32:42'!
dequeue
	
	| queueHandler |
	
	queueHandler _ DequeueAndNextHandler for: contents.
	
	^queueHandler dequeue: contents! !

!Queue methodsFor: 'operations' stamp: 'FR 10/3/2018 14:02:25'!
enqueue: anObjectToBeEnqueued
	
	^contents addLast: anObjectToBeEnqueued.! !

!Queue methodsFor: 'operations' stamp: 'FR 10/3/2018 14:02:32'!
isEmpty

	^contents isEmpty.! !

!Queue methodsFor: 'operations' stamp: 'IE 10/4/2018 16:32:42'!
next
	
	| queueHandler |
	
	queueHandler _ DequeueAndNextHandler for: contents.
	
	^queueHandler next: contents ! !

!Queue methodsFor: 'operations' stamp: 'FR 10/3/2018 14:02:55'!
size
	
	^contents size! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Queue class' category: #'Queue-Exercise'!
Queue class
	instanceVariableNames: ''!

!Queue class methodsFor: 'error descriptions' stamp: 'IE 10/3/2018 08:38:56'!
queueEmptyErrorDescription
	^ 'this Queue is empty'.! !
