!classDefinition: #TusLibrosTest category: #TusLibros!
TestCase subclass: #TusLibrosTest
	instanceVariableNames: 'tools merchantProcessorBlock'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!TusLibrosTest methodsFor: 'setUp' stamp: 'FR 11/19/2018 20:50:56'!
setUp

	tools _ TestTools new.
	merchantProcessorBlock _ [:amount :card |]! !


!classDefinition: #CartTest category: #TusLibros!
TusLibrosTest subclass: #CartTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CartTest methodsFor: 'tests' stamp: 'FR 11/17/2018 17:39:47'!
test01NewCartsAreCreatedEmpty

	self assert: tools createCart isEmpty! !

!CartTest methodsFor: 'tests' stamp: 'FR 11/17/2018 17:40:07'!
test02CanNotAddItemsThatDoNotBelongToStore

	| cart |
	
	cart := tools createCart.
	
	self 
		should: [ cart add: tools itemNotSoldByTheStore ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidItemErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'FR 11/17/2018 17:40:20'!
test03AfterAddingAnItemTheCartIsNotEmptyAnymore

	| cart |
	
	cart := tools createCart.
	
	cart add: tools itemSoldByTheStore.
	self deny: cart isEmpty ! !

!CartTest methodsFor: 'tests' stamp: 'FR 11/17/2018 17:40:27'!
test04CanNotAddNonPositiveNumberOfItems

	| cart |
	
	cart := tools createCart.
	
	self 
		should: [cart add: 0 of: tools itemSoldByTheStore ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidQuantityErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'FR 11/17/2018 17:40:52'!
test05CanNotAddMoreThanOneItemNotSoldByTheStore

	| cart |
	
	cart := tools createCart.
	
	self 
		should: [cart add: 2 of: tools itemNotSoldByTheStore  ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidItemErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'FR 11/17/2018 17:41:00'!
test06CartRemembersAddedItems

	| cart |
	
	cart := tools createCart.
	
	cart add: tools itemSoldByTheStore.
	self assert: (cart includes: tools itemSoldByTheStore)! !

!CartTest methodsFor: 'tests' stamp: 'FR 11/17/2018 17:41:10'!
test07CartDoesNotHoldNotAddedItems

	| cart |
	
	cart := tools createCart.
	
	self deny: (cart includes: tools itemSoldByTheStore)! !

!CartTest methodsFor: 'tests' stamp: 'FR 11/17/2018 17:41:18'!
test08CartRemembersTheNumberOfAddedItems

	| cart |
	
	cart := tools createCart.
	
	cart add: 2 of: tools itemSoldByTheStore.
	self assert: (cart occurrencesOf: tools itemSoldByTheStore) = 2! !


!classDefinition: #CashierTest category: #TusLibros!
TusLibrosTest subclass: #CashierTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CashierTest methodsFor: 'tests' stamp: 'FR 11/19/2018 20:48:13'!
test01CanNotCheckOutEmptyCart

	| cart cashier creditCard debitAcknowledged |
	
	cart _ tools createCart.
	cashier _ tools createCashierWithMerchantProcessor: self.
	creditCard _ tools createCreditCard.
	debitAcknowledged _ false.
	merchantProcessorBlock _ [:amount :card | debitAcknowledged _ true].
		
	self should: [cashier checkOut: cart with: creditCard]
	raise: Error
	withExceptionDo: [:anError |
		self assert: cashier cantCheckOutEmptyCartErrorMessage = anError messageText.
		self assert: cashier isItemLogEmpty.
		self deny: debitAcknowledged
	].! !

!CashierTest methodsFor: 'tests' stamp: 'FR 11/19/2018 20:48:39'!
test02CashierReturnsTicketSummarizingItemsInCartAfterCheckOut

	| cart cashier creditCard ticket debitAcknowledged |
	
	cart _ tools createCart.
	cart add: tools itemSoldByTheStore.
	cashier _ tools createCashierWithMerchantProcessor: self.
	creditCard _ tools createCreditCard.
	debitAcknowledged _ false.
	
	merchantProcessorBlock _ [:amount :card | debitAcknowledged _ true].
	ticket _ cashier checkOut: cart with: creditCard.
	
	self assert: ticket equals: 2.
	self assert: (cashier numberOfSalesOf: tools itemSoldByTheStore) equals: 1.
	self assert: debitAcknowledged! !

!CashierTest methodsFor: 'tests' stamp: 'FR 11/19/2018 20:49:53'!
test03CashierLogsSalesOfAllItemsInCartAfterCheckOut

	| cart cashier creditCard ticket debitAcknowledged |
	
	cart _ tools createCart.
	cart add: 3 of: tools itemSoldByTheStore.
	cashier _ tools createCashierWithMerchantProcessor: self.
	creditCard _ tools createCreditCard.
	
	debitAcknowledged _ false.
	merchantProcessorBlock _ [:amount :card | debitAcknowledged _ true].
	ticket _ cashier checkOut: cart with: creditCard.
	
	self assert: ticket equals: 6.
	self assert: (cashier numberOfSalesOf: tools itemSoldByTheStore) equals: 3.
	self assert: debitAcknowledged! !

!CashierTest methodsFor: 'tests' stamp: 'FR 11/19/2018 20:49:21'!
test04CanNotCheckOutWithExpiredCreditCard

	| cart cashier creditCard tools debitAcknowledged |
	
	tools _ TestTools new.
	cart := tools createCart.
	cart add: 3 of: tools itemSoldByTheStore.
	cashier _ tools createCashierWithMerchantProcessor: self.
	creditCard _ CreditCard newFor: 'Jefferson Gutierritos' with: 455 validUpTo: (GregorianMonthOfYear year: 1999 month: January).
	debitAcknowledged _ false.
	merchantProcessorBlock _ [:amount :card | debitAcknowledged _ true].
	
	self should: [cashier checkOut: cart with: creditCard]
	raise: Error
	withExceptionDo: [:anError |
		self assert: cashier expiredCreditCardErrorMessage equals: anError messageText.
		self assert: cashier isItemLogEmpty.
		self deny: debitAcknowledged
	].! !

!CashierTest methodsFor: 'tests' stamp: 'FR 11/21/2018 15:34:57'!
test05CashierLogsSalesOfItemsAcrossMultipleCarts

	| cashier tools debitAcknowledged firstCart firstCreditCard secondCart secondCreditCard firstTicket secondTicket |
	
	tools _ TestTools new.
	firstCart _ tools createCart.
	secondCart _ tools createCart.
	firstCart add: 3 of: tools itemSoldByTheStore.
	secondCart add: 2 of: tools itemSoldByTheStore.
	
	cashier _ tools createCashierWithMerchantProcessor: self.
	firstCreditCard _ tools createCreditCard.
	secondCreditCard _ CreditCard newFor: 'Alvin Yaqui Tori' with: 2610 validUpTo: (GregorianMonthOfYear year: 2020 month: August).
	debitAcknowledged _ false.
	merchantProcessorBlock _ [:amount :card | debitAcknowledged _ true].
	
	firstTicket _ cashier checkOut: firstCart with: firstCreditCard.
	secondTicket _ cashier checkOut: secondCart with: secondCreditCard.
	
	self assert: firstTicket equals: 6.
	self assert: secondTicket equals: 4.
	self assert: (cashier numberOfSalesOf: tools itemSoldByTheStore) equals: 5.
	self assert: debitAcknowledged.! !


!CashierTest methodsFor: 'merchant processor' stamp: 'FR 11/19/2018 20:50:29'!
debit: anAmountOfMoney with: aCreditCard

	merchantProcessorBlock value: anAmountOfMoney value: aCreditCard! !


!classDefinition: #Cart category: #TusLibros!
Object subclass: #Cart
	instanceVariableNames: 'catalog items'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Cart methodsFor: 'error messages' stamp: 'FR 11/17/2018 17:30:41'!
invalidItemErrorMessage
	
	^ 'Item is not in catalog'! !

!Cart methodsFor: 'error messages' stamp: 'FR 11/17/2018 17:30:38'!
invalidQuantityErrorMessage
	
	^ 'Invalid number of items'! !


!Cart methodsFor: 'assertions' stamp: 'FR 11/15/2018 20:13:34'!
assertIsValidItem: anItem

	(catalog includesKey: anItem) ifFalse: [ self error: self invalidItemErrorMessage ]! !

!Cart methodsFor: 'assertions' stamp: 'FR 11/17/2018 17:31:24'!
assertIsValidQuantity: aQuantity

	((aQuantity isKindOf: Integer) and: [aQuantity strictlyPositive]) ifFalse: [ self error: self invalidQuantityErrorMessage ]! !


!Cart methodsFor: 'initialization' stamp: 'FR 11/15/2018 19:17:24'!
initializeAcceptingItemsOf: aCatalog

	catalog := aCatalog.
	items := Bag new.! !


!Cart methodsFor: 'queries' stamp: 'FR 11/17/2018 17:29:50'!
occurrencesOf: anItem

	^ items occurrencesOf: anItem  ! !

!Cart methodsFor: 'queries' stamp: 'FR 11/15/2018 20:34:01'!
summarizeItems

	^ items inject: 0 into: [:totalPrice :anItem | totalPrice + catalog at: anItem]! !


!Cart methodsFor: 'testing' stamp: 'FR 11/17/2018 17:30:47'!
includes: anItem

	^ items includes: anItem ! !

!Cart methodsFor: 'testing' stamp: 'FR 11/17/2018 17:30:35'!
isEmpty
	
	^ items isEmpty ! !


!Cart methodsFor: 'adding' stamp: 'HernanWilkinson 6/17/2013 17:44'!
add: anItem

	^ self add: 1 of: anItem ! !

!Cart methodsFor: 'adding' stamp: 'FR 11/15/2018 19:18:13'!
add: aQuantity of: anItem

	self assertIsValidQuantity: aQuantity.
	self assertIsValidItem: anItem.

	items add: anItem withOccurrences: aQuantity.! !


!Cart methodsFor: 'evaluating' stamp: 'FR 11/17/2018 19:27:05'!
itemsDo: aBlock
	
	^ items do: aBlock! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cart class' category: #TusLibros!
Cart class
	instanceVariableNames: ''!

!Cart class methodsFor: 'instance creation' stamp: 'HernanWilkinson 6/17/2013 17:48'!
acceptingItemsOf: aCatalog

	^self new initializeAcceptingItemsOf: aCatalog ! !


!classDefinition: #Cashier category: #TusLibros!
Object subclass: #Cashier
	instanceVariableNames: 'prices itemLog merchantProcessor'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Cashier methodsFor: 'log' stamp: 'FR 11/17/2018 18:21:49'!
expiredCreditCardErrorMessage
	
	^ 'Credit card is expired'! !

!Cashier methodsFor: 'log' stamp: 'FR 11/19/2018 14:08:33'!
isItemLogEmpty

	^ itemLog isEmpty
	! !

!Cashier methodsFor: 'log' stamp: 'FR 11/19/2018 14:15:42'!
numberOfSalesOf: anItem

	^ itemLog occurrencesOf: anItem 
	! !


!Cashier methodsFor: 'error messages' stamp: 'FR 11/15/2018 20:58:19'!
cantCheckOutEmptyCartErrorMessage
	
	^ 'An empty cart cannot be checked out'! !


!Cashier methodsFor: 'initialization' stamp: 'FR 11/21/2018 14:52:58'!
initializeWithPrices: aDictionaryOfPrices andMerchantProcessor: aMerchantProcessor
	
	prices _ aDictionaryOfPrices.
	itemLog _ OrderedCollection new.
	merchantProcessor _ aMerchantProcessor! !


!Cashier methodsFor: 'check out' stamp: 'FR 11/21/2018 15:05:18'!
checkOut: aCart with: aCreditCard
	
	| totalPrice |
	self assertIsNotEmptyCart: aCart.
	self assertIsValidCreditCard: aCreditCard.

	totalPrice _ aCart summarizeItems.
	merchantProcessor debit: totalPrice with: aCreditCard.
	
	aCart itemsDo: [:anItem | itemLog add: anItem].
	^ totalPrice! !


!Cashier methodsFor: 'assertions' stamp: 'FR 11/21/2018 14:57:33'!
assertIsNotEmptyCart: aCart

	(aCart isEmpty) ifTrue: [self error: self cantCheckOutEmptyCartErrorMessage]! !

!Cashier methodsFor: 'assertions' stamp: 'FR 11/21/2018 15:04:23'!
assertIsValidCreditCard: aCreditCard
	
	| expirationMonth |
	expirationMonth _ GregorianMonthOfYear year: 2018 month: November.
	(aCreditCard isValidAt: expirationMonth) ifFalse: [self error: self expiredCreditCardErrorMessage].! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cashier class' category: #TusLibros!
Cashier class
	instanceVariableNames: ''!

!Cashier class methodsFor: 'instance creation' stamp: 'FR 11/19/2018 20:42:39'!
newWithPrices: aDictionaryOfPrices andMerchantProcessor: aMerchantProcessor
	
	^ self new initializeWithPrices: aDictionaryOfPrices andMerchantProcessor: aMerchantProcessor ! !


!classDefinition: #CreditCard category: #TusLibros!
Object subclass: #CreditCard
	instanceVariableNames: 'owner number expiration'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CreditCard methodsFor: 'initialization' stamp: 'FR 11/19/2018 20:45:16'!
initializeFor: aName withNumber: aNumber validUpTo: aGregorianMonthOfYear 
	
	owner _ aName.
	number _ aNumber.
	expiration _ aGregorianMonthOfYear ! !


!CreditCard methodsFor: 'testing' stamp: 'FR 11/17/2018 18:28:53'!
isValidAt: aMonthOfTheYear
	
	^ (expiration = aMonthOfTheYear) or: [aMonthOfTheYear < expiration]! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CreditCard class' category: #TusLibros!
CreditCard class
	instanceVariableNames: ''!

!CreditCard class methodsFor: 'instance creation' stamp: 'FR 11/19/2018 20:45:16'!
newFor: aName with: aNumber validUpTo: aGregorianMonthOfYear 
	
	^ self new initializeFor: aName withNumber: aNumber validUpTo: aGregorianMonthOfYear ! !


!classDefinition: #TestTools category: #TusLibros!
Object subclass: #TestTools
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!TestTools methodsFor: 'creation' stamp: 'FR 11/15/2018 20:01:39'!
createCart
	
	^ Cart acceptingItemsOf: self defaultPriceList! !

!TestTools methodsFor: 'creation' stamp: 'FR 11/19/2018 20:46:25'!
createCashierWithMerchantProcessor: aMerchantProcessor

	^ Cashier newWithPrices: self defaultPriceList andMerchantProcessor: aMerchantProcessor ! !

!TestTools methodsFor: 'creation' stamp: 'FR 11/17/2018 17:49:31'!
createCreditCard

	^ CreditCard newFor: self defaultOwner with: self defaultCreditCardNumber validUpTo: self defaultExpirationMonth! !

!TestTools methodsFor: 'creation' stamp: 'FR 11/17/2018 17:49:04'!
createExpiredCreditCard

	^ CreditCard newFor: self defaultOwner with: self defaultCreditCardNumber validUpTo: self pastExpirationMonth! !


!TestTools methodsFor: 'default values' stamp: 'FR 11/17/2018 17:46:18'!
defaultCreditCardNumber

	^ 455! !

!TestTools methodsFor: 'default values' stamp: 'FR 11/17/2018 17:47:52'!
defaultExpirationMonth

	^ GregorianMonthOfYear year: 2020 month: January! !

!TestTools methodsFor: 'default values' stamp: 'FR 11/17/2018 17:45:58'!
defaultOwner

	^ 'Jefferson Gutierritos'! !

!TestTools methodsFor: 'default values' stamp: 'FR 11/15/2018 20:00:43'!
defaultPrice
	
	^ 2! !

!TestTools methodsFor: 'default values' stamp: 'FR 11/15/2018 20:01:13'!
defaultPriceList
	
	| catalog |
	catalog _ Dictionary new.
	catalog at: self itemSoldByTheStore put: self defaultPrice.
	^catalog! !

!TestTools methodsFor: 'default values' stamp: 'FR 11/15/2018 19:47:12'!
itemNotSoldByTheStore
	
	^ 'invalidBook'! !

!TestTools methodsFor: 'default values' stamp: 'FR 11/15/2018 19:47:32'!
itemSoldByTheStore
	
	^ 'validBook'! !

!TestTools methodsFor: 'default values' stamp: 'FR 11/17/2018 17:48:07'!
pastExpirationMonth

	^ GregorianMonthOfYear year: 1999 month: January! !
