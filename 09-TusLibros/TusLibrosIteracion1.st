!classDefinition: #TusLibrosTest category: #TusLibros!
TestCase subclass: #TusLibrosTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!TusLibrosTest methodsFor: 'testing' stamp: 'FR 11/12/2018 21:41:59'!
test01NewCartIsEmpty
	
	| cart |
	cart _ Cart newWith: OrderedCollection new.
	
	self assert: cart isEmpty.! !

!TusLibrosTest methodsFor: 'testing' stamp: 'FR 11/12/2018 21:42:45'!
test02AfterAddingABookToCartItIsNotEmpty
	
	| cart catalog |
	catalog _ OrderedCollection with: 'Las aventuras de Tom Sawyer'.
	cart _ Cart newWith: catalog.
	cart addToCart: 'Las aventuras de Tom Sawyer'.
	
	self deny: cart isEmpty.! !

!TusLibrosTest methodsFor: 'testing' stamp: 'FR 11/12/2018 21:43:44'!
test03BookAddedToCartIsInCart
	
	| cart book catalog |
	catalog _ OrderedCollection with: 'Las aventuras de Tom Sawyer'.
	cart _ Cart newWith: catalog.
	book _ 'Las aventuras de Tom Sawyer'.
	cart addToCart: book.
	
	self assert: (cart isInCart: book).! !

!TusLibrosTest methodsFor: 'testing' stamp: 'FR 11/12/2018 21:44:36'!
test04BookNotAddedToCartIsNotInCart
	
	| cart book catalog |
	catalog _ OrderedCollection with: 'Las aventuras de Tom Sawyer'.
	cart _ Cart newWith: catalog.
	book _ 'Las aventuras de Tom Sawyer'.

	self deny: (cart isInCart: book).! !

!TusLibrosTest methodsFor: 'testing' stamp: 'FR 11/13/2018 17:41:02'!
test05CartHasQuantityOfBookEqualToTheNumberOfTimesItWasAdded
	
	| cart firstBook secondBook catalog |
	
	firstBook _ 'Las aventuras de Tom Sawyer'.
	secondBook _ 'Kafka on the Shore'.
	catalog _ OrderedCollection with: firstBook with: secondBook.
	
	cart _ Cart newWith: catalog.
	cart addToCart: firstBook.
	cart addToCart: secondBook.
	cart addToCart: secondBook.
	
	self assert: (cart quantityOf: firstBook) equals: 1.
	self assert: (cart quantityOf: secondBook) equals: 2.! !

!TusLibrosTest methodsFor: 'testing' stamp: 'FR 11/13/2018 17:59:09'!
test07TotalQuantityOfBooksInCartEqualsTheSumOfTheNumberOfTimesEachWasAdded
	
	| cart firstBook secondBook catalog |
	
	firstBook _ 'Las aventuras de Tom Sawyer'.
	secondBook _ 'Kafka on the Shore'.
	catalog _ OrderedCollection with: firstBook with: secondBook.
	
	cart _ Cart newWith: catalog.
	cart addToCart: firstBook.
	cart addToCart: secondBook.
	cart addToCart: secondBook.
	
	self assert: cart totalQuantity equals: 3! !

!TusLibrosTest methodsFor: 'testing' stamp: 'FR 11/13/2018 18:00:07'!
test08OnlyBooksFromTheCatalogCanBeAddedToCart
	
	| cart firstBook secondBook catalog |
	
	firstBook _ 'Las aventuras de Tom Sawyer'.
	secondBook _ 'Kafka on the Shore'.
	catalog _ OrderedCollection with: firstBook.
	cart _ Cart newWith: catalog.
	
	self should: [cart addToCart: secondBook]
	raise: Error
	withExceptionDo: [:anInvalidCart |
		self assert: Cart canOnlyAddBooksInCatalogErrorMessage equals: anInvalidCart messageText]! !

!TusLibrosTest methodsFor: 'testing' stamp: 'FR 11/13/2018 18:05:48'!
test09CartHasQuantityOfBookEqualToTheSumOfTheNumberOfTimesItWasAddedInEachAddition
	
	| cart firstBook secondBook catalog |
	
	firstBook _ 'Las aventuras de Tom Sawyer'.
	secondBook _ 'Kafka on the Shore'.
	catalog _ OrderedCollection with: firstBook with: secondBook.
	
	cart _ Cart newWith: catalog.
	cart addToCart: secondBook .
	cart addToCart: firstBook times: 3.
	cart addToCart: secondBook times: 4.
	
	self assert: (cart quantityOf: firstBook) equals: 3.
	self assert: (cart quantityOf: secondBook) equals: 5.! !

!TusLibrosTest methodsFor: 'testing' stamp: 'FR 11/13/2018 18:28:58'!
test10CanOnlyAddBookAPositiveNumberOfTimes
	
	| cart firstBook secondBook catalog |
	
	firstBook _ 'Las aventuras de Tom Sawyer'.
	secondBook _ 'Kafka on the Shore'.
	catalog _ OrderedCollection with: firstBook with: secondBook.
	
	cart _ Cart newWith: catalog.
	
	self should: [cart addToCart: firstBook times: -3]
	raise: Error
	withExceptionDo: [:anInvalidCart |
		self assert: Cart canOnlyAddBookPositiveNumberOfTimesErrorMessage equals: anInvalidCart messageText]! !


!classDefinition: #Cart category: #TusLibros!
Object subclass: #Cart
	instanceVariableNames: 'books catalog'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Cart methodsFor: 'addition' stamp: 'FR 11/12/2018 21:37:25'!
addToCart: aBookISBN

	(catalog includes: aBookISBN) ifFalse: [self error: self class canOnlyAddBooksInCatalogErrorMessage ].
	books add: aBookISBN 
	
	! !

!Cart methodsFor: 'addition' stamp: 'FR 11/13/2018 18:31:55'!
addToCart: aBookISBN times: numberOfTimes 
	
	((numberOfTimes isKindOf: Integer) and: [numberOfTimes > 0]) ifFalse: [self error: self class canOnlyAddBookPositiveNumberOfTimesErrorMessage].
	numberOfTimes timesRepeat: [self addToCart: aBookISBN ]! !


!Cart methodsFor: 'initialization' stamp: 'FR 11/12/2018 21:34:56'!
initializeWith: aCatalog

	books _ OrderedCollection new.
	catalog _ aCatalog! !


!Cart methodsFor: 'testing' stamp: 'FR 11/12/2018 20:14:22'!
isEmpty

	^ books isEmpty! !

!Cart methodsFor: 'testing' stamp: 'FR 11/12/2018 20:25:22'!
isInCart: aBookISDN
	
	^ books includes: aBookISDN ! !


!Cart methodsFor: 'quantity' stamp: 'FR 11/12/2018 20:52:58'!
quantityOf: aBookISDN
	
	^ books occurrencesOf: aBookISDN ! !

!Cart methodsFor: 'quantity' stamp: 'FR 11/12/2018 21:11:09'!
totalQuantity
	
	^ books size! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cart class' category: #TusLibros!
Cart class
	instanceVariableNames: ''!

!Cart class methodsFor: 'error messages' stamp: 'FR 11/13/2018 18:32:44'!
canOnlyAddBookPositiveNumberOfTimesErrorMessage
	
	^ 'Books can only be added to a cart a positive number of times'! !

!Cart class methodsFor: 'error messages' stamp: 'FR 11/12/2018 21:37:10'!
canOnlyAddBooksInCatalogErrorMessage

	^ 'Only books in catalog can be added to cart' ! !


!Cart class methodsFor: 'instance creation' stamp: 'FR 11/12/2018 21:34:33'!
newWith: aCatalog

	^ self new initializeWith: aCatalog! !
