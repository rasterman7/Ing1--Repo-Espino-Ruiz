!classDefinition: #AccountTransaction category: #'PortfolioTreePrinter-Ejercicio'!
Object subclass: #AccountTransaction
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PortfolioTreePrinter-Ejercicio'!

!AccountTransaction methodsFor: 'evaluating' stamp: 'HAW 10/18/2018 20:20:37'!
affectBalance: currentBalance  

	self subclassResponsibility ! !

!AccountTransaction methodsFor: 'evaluating' stamp: 'HernanWilkinson 7/14/2011 06:48'!
value

	self subclassResponsibility ! !


!AccountTransaction methodsFor: 'visitor' stamp: 'FR 10/22/2018 20:37:39'!
accept: aVisitor

	self subclassResponsibility ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'AccountTransaction class' category: #'PortfolioTreePrinter-Ejercicio'!
AccountTransaction class
	instanceVariableNames: ''!

!AccountTransaction class methodsFor: 'instance creation' stamp: 'HAW 10/18/2018 21:06:37'!
for: anAmount

	self subclassResponsibility ! !


!AccountTransaction class methodsFor: 'registering' stamp: 'HAW 10/18/2018 19:48:18'!
register: aValue on: account

	| transaction |
	
	transaction := self for: aValue.
	account register: transaction.
		
	^ transaction! !


!classDefinition: #CertificateOfDeposit category: #'PortfolioTreePrinter-Ejercicio'!
AccountTransaction subclass: #CertificateOfDeposit
	instanceVariableNames: 'value days tna'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PortfolioTreePrinter-Ejercicio'!

!CertificateOfDeposit methodsFor: 'initialization' stamp: 'FR 10/22/2018 19:00:51'!
initializeFor: anAmount during: aNumberOfDays at: anInterestRate
	
	value _ anAmount.
	days _ aNumberOfDays.
	tna _ anInterestRate ! !


!CertificateOfDeposit methodsFor: 'visitor' stamp: 'FR 10/22/2018 19:01:41'!
accept: aVisitor

	aVisitor visitCertificateOfDeposit: self! !


!CertificateOfDeposit methodsFor: 'evaluating' stamp: 'FR 10/21/2018 11:32:17'!
affectBalance: currentBalance  

	^ currentBalance - self value! !

!CertificateOfDeposit methodsFor: 'evaluating' stamp: 'FR 10/22/2018 18:56:06'!
value

	^ value! !


!CertificateOfDeposit methodsFor: 'certificate' stamp: 'FR 10/22/2018 18:51:13'!
days

	^ days! !

!CertificateOfDeposit methodsFor: 'certificate' stamp: 'FR 10/22/2018 18:51:34'!
interestRate

	^ tna! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CertificateOfDeposit class' category: #'PortfolioTreePrinter-Ejercicio'!
CertificateOfDeposit class
	instanceVariableNames: ''!

!CertificateOfDeposit class methodsFor: 'instance creation' stamp: 'FR 10/22/2018 19:00:57'!
for: anAmount during: aNumberOfDays at: anInterestRate

	^ self new initializeFor: anAmount during: aNumberOfDays at: anInterestRate ! !


!CertificateOfDeposit class methodsFor: 'registering' stamp: 'FR 10/22/2018 19:41:18'!
register: anAmount during: aNumberOfDays at: anInterestRate on: anAccount

	| certificate |
	certificate _ self for: anAmount during: aNumberOfDays at: anInterestRate.
	anAccount register: certificate.
	^ certificate ! !


!classDefinition: #Deposit category: #'PortfolioTreePrinter-Ejercicio'!
AccountTransaction subclass: #Deposit
	instanceVariableNames: 'value'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PortfolioTreePrinter-Ejercicio'!

!Deposit methodsFor: 'initialization' stamp: 'HernanWilkinson 7/13/2011 18:45'!
initializeFor: aValue

	value := aValue ! !


!Deposit methodsFor: 'evaluating' stamp: 'HAW 10/18/2018 20:37:28'!
affectBalance: currentBalance  

	^currentBalance + self value! !

!Deposit methodsFor: 'evaluating' stamp: 'HernanWilkinson 7/13/2011 18:38'!
value

	^ value! !


!Deposit methodsFor: 'visitor' stamp: 'FR 10/22/2018 18:06:54'!
accept: aVisitor

	aVisitor visitDeposit: self! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Deposit class' category: #'PortfolioTreePrinter-Ejercicio'!
Deposit class
	instanceVariableNames: ''!

!Deposit class methodsFor: 'instance creation' stamp: 'HernanWilkinson 7/13/2011 18:38'!
for: aValue

	^ self new initializeFor: aValue ! !


!classDefinition: #TransferLeg category: #'PortfolioTreePrinter-Ejercicio'!
AccountTransaction subclass: #TransferLeg
	instanceVariableNames: 'transfer'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PortfolioTreePrinter-Ejercicio'!

!TransferLeg methodsFor: 'transfer' stamp: 'HAW 10/18/2018 21:05:34'!
transfer

	^transfer ! !


!TransferLeg methodsFor: 'initialization' stamp: 'HAW 10/18/2018 21:07:15'!
initializeRelatedTo: aTransfer 
	
	transfer := aTransfer ! !


!TransferLeg methodsFor: 'evaluating' stamp: 'HAW 10/18/2018 20:35:56'!
value

	^transfer value! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'TransferLeg class' category: #'PortfolioTreePrinter-Ejercicio'!
TransferLeg class
	instanceVariableNames: ''!

!TransferLeg class methodsFor: 'instance creation' stamp: 'HAW 10/18/2018 21:07:07'!
relatedTo: aTransfer 
	
	^self new initializeRelatedTo: aTransfer ! !


!classDefinition: #TransferDeposit category: #'PortfolioTreePrinter-Ejercicio'!
TransferLeg subclass: #TransferDeposit
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PortfolioTreePrinter-Ejercicio'!

!TransferDeposit methodsFor: 'evaluating' stamp: 'HAW 10/18/2018 20:34:58'!
affectBalance: currentBalance

	^currentBalance + self value! !


!TransferDeposit methodsFor: 'visitor' stamp: 'FR 10/22/2018 18:07:28'!
accept: aVisitor

	aVisitor visitTransferDeposit: self! !


!classDefinition: #TransferWithdraw category: #'PortfolioTreePrinter-Ejercicio'!
TransferLeg subclass: #TransferWithdraw
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PortfolioTreePrinter-Ejercicio'!

!TransferWithdraw methodsFor: 'evaluating' stamp: 'HAW 10/18/2018 20:34:40'!
affectBalance: currentBalance

	^currentBalance - self value! !


!TransferWithdraw methodsFor: 'visitor' stamp: 'FR 10/22/2018 18:07:40'!
accept: aVisitor

	aVisitor visitTransferWithdraw: self! !


!classDefinition: #Withdraw category: #'PortfolioTreePrinter-Ejercicio'!
AccountTransaction subclass: #Withdraw
	instanceVariableNames: 'value'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PortfolioTreePrinter-Ejercicio'!

!Withdraw methodsFor: 'initialization' stamp: 'HernanWilkinson 7/13/2011 18:46'!
initializeFor: aValue

	value := aValue ! !


!Withdraw methodsFor: 'evaluating' stamp: 'HAW 10/18/2018 20:36:35'!
affectBalance: currentBalance  

	^currentBalance - self value! !

!Withdraw methodsFor: 'evaluating' stamp: 'HernanWilkinson 7/14/2011 05:54'!
value

	^ value ! !


!Withdraw methodsFor: 'visitor' stamp: 'FR 10/22/2018 18:07:58'!
accept: aVisitor

	aVisitor visitWithdraw: self! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Withdraw class' category: #'PortfolioTreePrinter-Ejercicio'!
Withdraw class
	instanceVariableNames: ''!

!Withdraw class methodsFor: 'instance creation' stamp: 'HernanWilkinson 7/13/2011 18:33'!
for: aValue

	^ self new initializeFor: aValue ! !


!classDefinition: #AccountTransactionVisitor category: #'PortfolioTreePrinter-Ejercicio'!
Object subclass: #AccountTransactionVisitor
	instanceVariableNames: 'account'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PortfolioTreePrinter-Ejercicio'!

!AccountTransactionVisitor methodsFor: 'initialization' stamp: 'FR 10/22/2018 19:16:45'!
initializeOf: aReceptiveAccount

	account _ aReceptiveAccount ! !


!AccountTransactionVisitor methodsFor: 'evaluating' stamp: 'FR 10/22/2018 19:11:13'!
value

	self subclassResponsibility ! !

!AccountTransactionVisitor methodsFor: 'evaluating' stamp: 'FR 10/22/2018 19:03:06'!
visitCertificateOfDeposit: aCertificateOfDeposit

	self subclassResponsibility ! !

!AccountTransactionVisitor methodsFor: 'evaluating' stamp: 'FR 10/22/2018 18:25:05'!
visitDeposit: aDeposit

	self subclassResponsibility ! !

!AccountTransactionVisitor methodsFor: 'evaluating' stamp: 'FR 10/22/2018 18:27:52'!
visitTransferDeposit: aTransferDeposit

	self subclassResponsibility ! !

!AccountTransactionVisitor methodsFor: 'evaluating' stamp: 'FR 10/22/2018 18:27:16'!
visitTransferWithdraw: aTransferWithdraw

	self subclassResponsibility ! !

!AccountTransactionVisitor methodsFor: 'evaluating' stamp: 'FR 10/22/2018 18:25:25'!
visitWithdraw: aWithdraw

	self subclassResponsibility ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'AccountTransactionVisitor class' category: #'PortfolioTreePrinter-Ejercicio'!
AccountTransactionVisitor class
	instanceVariableNames: ''!

!AccountTransactionVisitor class methodsFor: 'instance creation' stamp: 'FR 10/22/2018 19:36:33'!
of: aReceptiveAccount

	^ self new initializeOf: aReceptiveAccount! !


!classDefinition: #AccountInvestmentEarnings category: #'PortfolioTreePrinter-Ejercicio'!
AccountTransactionVisitor subclass: #AccountInvestmentEarnings
	instanceVariableNames: 'investmentEarnings'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PortfolioTreePrinter-Ejercicio'!

!AccountInvestmentEarnings methodsFor: 'evaluating' stamp: 'FR 10/22/2018 19:43:08'!
value

	investmentEarnings _ 0.	
	account visitTransactionsWith: self.
	^ investmentEarnings
	! !

!AccountInvestmentEarnings methodsFor: 'evaluating' stamp: 'FR 10/22/2018 19:18:56'!
visitCertificateOfDeposit: aCertificateOfDeposit

	investmentEarnings _ investmentEarnings + (aCertificateOfDeposit value * aCertificateOfDeposit interestRate * aCertificateOfDeposit days / 360)! !

!AccountInvestmentEarnings methodsFor: 'evaluating' stamp: 'FR 10/22/2018 19:23:29'!
visitDeposit: aDeposit! !

!AccountInvestmentEarnings methodsFor: 'evaluating' stamp: 'FR 10/22/2018 19:23:47'!
visitTransferDeposit: aTransferDeposit! !

!AccountInvestmentEarnings methodsFor: 'evaluating' stamp: 'FR 10/22/2018 19:24:28'!
visitTransferWithdraw: aTransferWithdraw! !

!AccountInvestmentEarnings methodsFor: 'evaluating' stamp: 'FR 10/22/2018 19:24:13'!
visitWithdraw: aWithdraw! !


!classDefinition: #AccountInvestmentNet category: #'PortfolioTreePrinter-Ejercicio'!
AccountTransactionVisitor subclass: #AccountInvestmentNet
	instanceVariableNames: 'investmentNet'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PortfolioTreePrinter-Ejercicio'!

!AccountInvestmentNet methodsFor: 'evaluating' stamp: 'FR 10/22/2018 19:16:01'!
value

	investmentNet _ 0.
	account visitTransactionsWith: self.
	
	^investmentNet ! !

!AccountInvestmentNet methodsFor: 'evaluating' stamp: 'FR 10/22/2018 19:14:40'!
visitCertificateOfDeposit: aCertificateOfDeposit

	investmentNet _ investmentNet + aCertificateOfDeposit value! !

!AccountInvestmentNet methodsFor: 'evaluating' stamp: 'FR 10/22/2018 19:13:04'!
visitDeposit: aDeposit! !

!AccountInvestmentNet methodsFor: 'evaluating' stamp: 'FR 10/22/2018 19:13:40'!
visitTransferDeposit: aTransferDeposit! !

!AccountInvestmentNet methodsFor: 'evaluating' stamp: 'FR 10/22/2018 19:13:29'!
visitTransferWithdraw: aTransferWithdraw! !

!AccountInvestmentNet methodsFor: 'evaluating' stamp: 'FR 10/22/2018 19:13:17'!
visitWithdraw: aWithdraw! !


!classDefinition: #AccountSummaryLines category: #'PortfolioTreePrinter-Ejercicio'!
AccountTransactionVisitor subclass: #AccountSummaryLines
	instanceVariableNames: 'lines'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PortfolioTreePrinter-Ejercicio'!

!AccountSummaryLines methodsFor: 'evaluating' stamp: 'FR 10/22/2018 19:42:42'!
value

	lines _ OrderedCollection new.
	account visitTransactionsWith: self.
	^ lines! !

!AccountSummaryLines methodsFor: 'evaluating' stamp: 'FR 10/22/2018 18:57:59'!
visitCertificateOfDeposit: aCertificateOfDeposit

	lines add: 'Plazo fijo por ', aCertificateOfDeposit value printString, ' durante ', aCertificateOfDeposit days printString, ' dias a una tna de ', (1/aCertificateOfDeposit interestRate) printString, '%'

	! !

!AccountSummaryLines methodsFor: 'evaluating' stamp: 'FR 10/22/2018 18:10:22'!
visitDeposit: aDeposit

	lines add: 'Deposito por ', aDeposit value printString! !

!AccountSummaryLines methodsFor: 'evaluating' stamp: 'FR 10/22/2018 18:13:18'!
visitTransferDeposit: aTransferDeposit

	lines add: 'Transferencia por ', aTransferDeposit value printString! !

!AccountSummaryLines methodsFor: 'evaluating' stamp: 'FR 10/22/2018 18:13:09'!
visitTransferWithdraw: aTransferWithdraw

	lines add: 'Transferencia por ', aTransferWithdraw value negated printString! !

!AccountSummaryLines methodsFor: 'evaluating' stamp: 'FR 10/22/2018 18:13:52'!
visitWithdraw: aWithdraw

	lines add: 'Extraccion por ', aWithdraw value printString! !


!classDefinition: #AccountTransferNet category: #'PortfolioTreePrinter-Ejercicio'!
AccountTransactionVisitor subclass: #AccountTransferNet
	instanceVariableNames: 'transferNet'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PortfolioTreePrinter-Ejercicio'!

!AccountTransferNet methodsFor: 'evaluating' stamp: 'FR 10/22/2018 19:43:23'!
value

	transferNet _ 0.
	account visitTransactionsWith: self.
	^ transferNet ! !

!AccountTransferNet methodsFor: 'evaluating' stamp: 'FR 10/22/2018 19:04:23'!
visitCertificateOfDeposit: aCertificateOfDeposit! !

!AccountTransferNet methodsFor: 'evaluating' stamp: 'FR 10/22/2018 18:14:52'!
visitDeposit: aDeposit! !

!AccountTransferNet methodsFor: 'evaluating' stamp: 'FR 10/22/2018 18:16:05'!
visitTransferDeposit: aTransferDeposit

	transferNet _ transferNet + aTransferDeposit value! !

!AccountTransferNet methodsFor: 'evaluating' stamp: 'FR 10/22/2018 18:15:40'!
visitTransferWithdraw: aTransferWithdraw

	transferNet _ transferNet - aTransferWithdraw value! !

!AccountTransferNet methodsFor: 'evaluating' stamp: 'FR 10/22/2018 18:15:07'!
visitWithdraw: aWithdraw! !


!classDefinition: #PortfolioTreePrinterVisitor category: #'PortfolioTreePrinter-Ejercicio'!
Object subclass: #PortfolioTreePrinterVisitor
	instanceVariableNames: 'portfolio dictionary spacesByDepth lines'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PortfolioTreePrinter-Ejercicio'!

!PortfolioTreePrinterVisitor methodsFor: 'evaluating' stamp: 'FR 10/22/2018 23:29:38'!
value

	self subclassResponsibility ! !

!PortfolioTreePrinterVisitor methodsFor: 'evaluating' stamp: 'FR 10/22/2018 20:51:13'!
visitPortfolio: aPortfolio

	lines add: spacesByDepth, (dictionary at: aPortfolio).
	spacesByDepth _ spacesByDepth, (String new: 1) atAllPut: $ .
	aPortfolio visitAccountCollectionWith: self.
	spacesByDepth _ spacesByDepth copyUpToLast: $ .! !

!PortfolioTreePrinterVisitor methodsFor: 'evaluating' stamp: 'FR 10/22/2018 20:29:12'!
visitReceptiveAccount: aReceptiveAccount

	lines add: spacesByDepth, (dictionary at: aReceptiveAccount).! !


!PortfolioTreePrinterVisitor methodsFor: 'initialization' stamp: 'FR 10/22/2018 20:02:52'!
initializeOf: aPortfolio with: aDictionary

	portfolio _ aPortfolio.
	dictionary _ aDictionary ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'PortfolioTreePrinterVisitor class' category: #'PortfolioTreePrinter-Ejercicio'!
PortfolioTreePrinterVisitor class
	instanceVariableNames: ''!

!PortfolioTreePrinterVisitor class methodsFor: 'instance creation' stamp: 'FR 10/22/2018 20:02:14'!
of: aPortfolio with: aDictionary

	^ self new initializeOf: aPortfolio with: aDictionary! !


!classDefinition: #FirstOrderPortfolioTreePrinterVisitor category: #'PortfolioTreePrinter-Ejercicio'!
PortfolioTreePrinterVisitor subclass: #FirstOrderPortfolioTreePrinterVisitor
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PortfolioTreePrinter-Ejercicio'!

!FirstOrderPortfolioTreePrinterVisitor methodsFor: 'evaluating' stamp: 'FR 10/22/2018 23:30:47'!
value

	spacesByDepth _ ''.
	lines _ OrderedCollection new.
	portfolio accept: self.
	^ lines! !


!classDefinition: #ReversedPortfolioTreePrinterVisitor category: #'PortfolioTreePrinter-Ejercicio'!
PortfolioTreePrinterVisitor subclass: #ReversedPortfolioTreePrinterVisitor
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PortfolioTreePrinter-Ejercicio'!

!ReversedPortfolioTreePrinterVisitor methodsFor: 'evaluating' stamp: 'FR 10/22/2018 23:29:05'!
value

	spacesByDepth _ ''.
	lines _ OrderedCollection new.
	portfolio accept: self.
	^ lines reversed! !


!classDefinition: #SummarizingAccount category: #'PortfolioTreePrinter-Ejercicio'!
Object subclass: #SummarizingAccount
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PortfolioTreePrinter-Ejercicio'!

!SummarizingAccount methodsFor: 'testing' stamp: 'HAW 10/18/2018 16:31:24'!
doesManage: anAccount

	self subclassResponsibility ! !

!SummarizingAccount methodsFor: 'testing' stamp: 'HAW 10/18/2018 16:32:05'!
hasRegistered: aTransaction

	self subclassResponsibility ! !


!SummarizingAccount methodsFor: 'transactions' stamp: 'HernanWilkinson 7/13/2011 18:34'!
balance

	self subclassResponsibility ! !

!SummarizingAccount methodsFor: 'transactions' stamp: 'HernanWilkinson 7/13/2011 18:35'!
transactions

	self subclassResponsibility ! !


!SummarizingAccount methodsFor: 'visitor' stamp: 'FR 10/22/2018 20:04:00'!
accept: aVisitor

	self subclassResponsibility ! !


!classDefinition: #Portfolio category: #'PortfolioTreePrinter-Ejercicio'!
SummarizingAccount subclass: #Portfolio
	instanceVariableNames: 'accounts'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PortfolioTreePrinter-Ejercicio'!

!Portfolio methodsFor: 'testing' stamp: 'HAW 10/18/2018 16:31:24'!
anyManagedAccountManages: anAccount 

	^accounts anySatisfy: [ :managedAccount | (managedAccount doesManage: anAccount) or: [ anAccount doesManage: managedAccount ] ] ! !

!Portfolio methodsFor: 'testing' stamp: 'HAW 10/18/2018 16:31:24'!
doesManage: anAccount

	^ self = anAccount  or: [ self anyManagedAccountManages: anAccount ]! !

!Portfolio methodsFor: 'testing' stamp: 'HAW 10/18/2018 16:32:05'!
hasRegistered: aTransaction

	^ accounts anySatisfy: [ :anAccount | anAccount hasRegistered: aTransaction ]  ! !


!Portfolio methodsFor: 'transactions' stamp: 'HAW 8/13/2017 17:47:44'!
balance

	^ accounts sum: [ :account | account balance ]
! !

!Portfolio methodsFor: 'transactions' stamp: 'HAW 9/25/2017 19:40:20'!
transactions 

	^ accounts 
		inject: OrderedCollection new 
		into: [ :transactions :account | transactions addAll: account transactions. transactions ]
	! !

!Portfolio methodsFor: 'transactions' stamp: 'FR 10/22/2018 18:29:23'!
transactionsDo: aBlock

	accounts transactionsDo: [:anAccount | anAccount transactionsDo: aBlock ].! !

!Portfolio methodsFor: 'transactions' stamp: 'HAW 10/18/2018 16:31:24'!
transactionsOf: anAccount 

	^ (self doesManage: anAccount)
		ifTrue: [ anAccount transactions ] 
		ifFalse: [ self error: self class accountNotManagedMessageDescription]
	! !


!Portfolio methodsFor: 'initialization' stamp: 'HernanWilkinson 7/13/2011 19:19'!
initializeWithAll: aCollectionOfAccounts

	accounts := aCollectionOfAccounts   ! !


!Portfolio methodsFor: 'visitor' stamp: 'FR 10/22/2018 20:04:31'!
accept: aVisitor

	aVisitor visitPortfolio: self! !

!Portfolio methodsFor: 'visitor' stamp: 'FR 10/22/2018 20:52:09'!
visitAccountCollectionWith: aVisitor

	self accountCollectionDo: [:anAccount | anAccount accept: aVisitor].! !

!Portfolio methodsFor: 'visitor' stamp: 'FR 10/22/2018 18:40:41'!
visitTransactionsWith: aVisitor

	self transactionsDo: [:aTransaction | aTransaction accept: aVisitor].! !


!Portfolio methodsFor: 'accounts' stamp: 'FR 10/22/2018 20:12:46'!
accountCollection

	^ accounts   ! !

!Portfolio methodsFor: 'accounts' stamp: 'FR 10/22/2018 20:53:42'!
accountCollectionDo: aBlock

	accounts do: aBlock    ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Portfolio class' category: #'PortfolioTreePrinter-Ejercicio'!
Portfolio class
	instanceVariableNames: ''!

!Portfolio class methodsFor: 'instance creation' stamp: 'HAW 5/8/2018 16:33:13'!
with: leftAccount with: rightAccount

	^ self withAll: (Array with: leftAccount with: rightAccount)! !

!Portfolio class methodsFor: 'instance creation' stamp: 'HAW 5/8/2018 16:40:55'!
withAll: aCollectionOfAccounts

	self checkCreationPreconditions: aCollectionOfAccounts.
	
	^self new initializeWithAll: aCollectionOfAccounts ! !


!Portfolio class methodsFor: 'assertions' stamp: 'HAW 10/18/2018 16:31:24'!
check: sourceAccount doesNotManagaAnyOf: aCollectionOfAccounts

	^ aCollectionOfAccounts do: [ :targetAccount | 
			(sourceAccount = targetAccount) not ifTrue: [ 
				(sourceAccount doesManage: targetAccount) ifTrue: [ self error: self accountAlreadyManagedErrorMessage ] ] ]! !

!Portfolio class methodsFor: 'assertions' stamp: 'HernanWilkinson 9/18/2011 17:21'!
checkAccountsAreUnique: aCollectionOfAccounts

	aCollectionOfAccounts asSet size = aCollectionOfAccounts size
		ifFalse: [ self error: self accountAlreadyManagedErrorMessage ]! !

!Portfolio class methodsFor: 'assertions' stamp: 'HernanWilkinson 9/18/2011 17:21'!
checkCreationPreconditions: aCollectionOfAccounts

	self checkAccountsAreUnique: aCollectionOfAccounts.
	self checkNoCircularReferencesIn: aCollectionOfAccounts! !

!Portfolio class methodsFor: 'assertions' stamp: 'HernanWilkinson 9/18/2011 17:22'!
checkNoCircularReferencesIn: aCollectionOfAccounts

	aCollectionOfAccounts do: [ :sourceAccount | self check: sourceAccount doesNotManagaAnyOf: aCollectionOfAccounts ]! !


!Portfolio class methodsFor: 'error messages' stamp: 'HernanWilkinson 7/13/2011 19:28'!
accountAlreadyManagedErrorMessage

	^ 'Account already managed'! !

!Portfolio class methodsFor: 'error messages' stamp: 'HernanWilkinson 7/13/2011 19:27'!
accountNotManagedMessageDescription
	
	^ 'Account not managed'! !


!classDefinition: #ReceptiveAccount category: #'PortfolioTreePrinter-Ejercicio'!
SummarizingAccount subclass: #ReceptiveAccount
	instanceVariableNames: 'transactions'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PortfolioTreePrinter-Ejercicio'!

!ReceptiveAccount methodsFor: 'initialization' stamp: 'HernanWilkinson 7/13/2011 18:35'!
initialize

	super initialize.
	transactions := OrderedCollection new.! !


!ReceptiveAccount methodsFor: 'transactions' stamp: 'HAW 10/18/2018 20:20:37'!
balance

	^ transactions inject: 0 into: [ :balance :transaction | transaction affectBalance: balance ]! !

!ReceptiveAccount methodsFor: 'transactions' stamp: 'HernanWilkinson 7/13/2011 18:37'!
register: aTransaction

	transactions add: aTransaction 
! !

!ReceptiveAccount methodsFor: 'transactions' stamp: 'HernanWilkinson 7/13/2011 18:37'!
transactions 

	^ transactions copy! !

!ReceptiveAccount methodsFor: 'transactions' stamp: 'FR 10/22/2018 18:29:55'!
transactionsDo: aBlock

	transactions do: aBlock.! !


!ReceptiveAccount methodsFor: 'testing' stamp: 'HAW 10/18/2018 16:31:24'!
doesManage: anAccount

	^ self = anAccount 
! !

!ReceptiveAccount methodsFor: 'testing' stamp: 'HAW 10/18/2018 16:32:05'!
hasRegistered: aTtransaction

	^ transactions includes: aTtransaction 
! !


!ReceptiveAccount methodsFor: 'visitor' stamp: 'FR 10/22/2018 20:04:58'!
accept: aVisitor

	aVisitor visitReceptiveAccount: self! !

!ReceptiveAccount methodsFor: 'visitor' stamp: 'FR 10/22/2018 18:23:02'!
visitTransactionsWith: aVisitor

	transactions do: [:aTransaction | aTransaction accept: aVisitor].! !


!classDefinition: #Transfer category: #'PortfolioTreePrinter-Ejercicio'!
Object subclass: #Transfer
	instanceVariableNames: 'value depositLeg withdrawLeg'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PortfolioTreePrinter-Ejercicio'!

!Transfer methodsFor: 'initialization' stamp: 'FR 10/18/2018 21:23:45'!
initializeOf: anAmount 
	
	value := anAmount.
	depositLeg := TransferDeposit relatedTo: self.
	withdrawLeg := TransferWithdraw relatedTo: self 
	
	! !


!Transfer methodsFor: 'legs' stamp: 'HAW 10/18/2018 20:53:34'!
depositLeg
	
	^depositLeg! !

!Transfer methodsFor: 'legs' stamp: 'HAW 10/18/2018 20:53:26'!
withdrawLeg
	
	^withdrawLeg! !


!Transfer methodsFor: 'evaluating' stamp: 'HAW 10/18/2018 21:08:33'!
value

	^value ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Transfer class' category: #'PortfolioTreePrinter-Ejercicio'!
Transfer class
	instanceVariableNames: ''!

!Transfer class methodsFor: 'instance creation' stamp: 'HAW 10/18/2018 21:07:39'!
of: anAmount 

	^self new initializeOf: anAmount ! !


!Transfer class methodsFor: 'registering' stamp: 'FR 10/18/2018 21:24:06'!
register: anAmount from: fromAccount to: toAccount 

	|transfer |
	
	transfer := self of: anAmount.
	fromAccount register: transfer withdrawLeg.
	toAccount register: transfer depositLeg.
	
	^transfer! !
