!classDefinition: #I category: #'Tercera actividad'!
DenotativeObject subclass: #I
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Tercera actividad'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'I class' category: #'Tercera actividad'!
I class
	instanceVariableNames: 'next'!

!I class methodsFor: 'as yet unclassified' stamp: 'FR 9/9/2018 19:42:59'!
* unNumeroDePeano

	^ unNumeroDePeano	! !

!I class methodsFor: 'as yet unclassified' stamp: 'FR 9/6/2018 21:41:22'!
+ unNumeroDePeano

	^ unNumeroDePeano next! !

!I class methodsFor: 'as yet unclassified' stamp: 'FR 9/12/2018 18:18:20'!
- unNumeroDePeano

	^self error: self descripcionDeErrorDeNumerosNegativosNoSoportados.! !

!I class methodsFor: 'as yet unclassified' stamp: 'FR 9/12/2018 18:18:02'!
/ unNumeroDePeano

	(unNumeroDePeano = I) ifFalse: [self error: self descripcionDeErrorDeNoSePuedeDividirPorUnNumeroMayor].
	(unNumeroDePeano = I) ifTrue: [^I].! !

!I class methodsFor: 'as yet unclassified' stamp: 'FR 9/9/2018 20:47:39'!
<= unNumeroDePeano

	^true! !

!I class methodsFor: 'as yet unclassified' stamp: 'FR 9/12/2018 18:16:08'!
descripcionDeErrorDeNoSePuedeDividirPorUnNumeroMayor

	^'No se puede dividir por un n�mero de Peano mayor'.! !

!I class methodsFor: 'as yet unclassified' stamp: 'FR 9/15/2018 15:59:56'!
descripcionDeErrorDeNumerosNegativosNoSoportados
	"Este mensaje de error tambi�n aparece en el caso de efectuar la resta entre dos n�meros de Peano de igual valor"
	
	^'Los n�meros negativos no est�n soportados por el modelo de n�meros de Peano'.! !

!I class methodsFor: 'as yet unclassified' stamp: 'FR 9/17/2018 13:57:25'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := II.! !

!I class methodsFor: 'as yet unclassified' stamp: 'FR 9/6/2018 21:25:16'!
next

	^II! !

!I class methodsFor: 'as yet unclassified' stamp: 'FR 9/12/2018 20:56:01'!
restaleA: unNumeroDePeano

	^unNumeroDePeano previous! !


!classDefinition: #II category: #'Tercera actividad'!
DenotativeObject subclass: #II
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Tercera actividad'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'II class' category: #'Tercera actividad'!
II class
	instanceVariableNames: 'previous next'!

!II class methodsFor: 'as yet unclassified' stamp: 'FR 9/12/2018 16:59:40'!
* unNumeroDePeano
	
	^ self previous * unNumeroDePeano + unNumeroDePeano! !

!II class methodsFor: 'as yet unclassified' stamp: 'FR 9/12/2018 16:56:21'!
+ unNumeroDePeano

	^ (self previous) + unNumeroDePeano next! !

!II class methodsFor: 'as yet unclassified' stamp: 'FR 9/12/2018 20:56:46'!
- unNumeroDePeano
	
	^unNumeroDePeano restaleA: self.! !

!II class methodsFor: 'as yet unclassified' stamp: 'FR 9/17/2018 13:57:08'!
/ unNumeroDePeano

	(unNumeroDePeano <= self) ifFalse: [self error: self descripcionDeErrorDeNoSePuedeDividirPorUnNumeroMayor].
	(self <= ((unNumeroDePeano + unNumeroDePeano) previous)) ifFalse: [^((self - unNumeroDePeano) / unNumeroDePeano) next].
	^I.
	! !

!II class methodsFor: 'as yet unclassified' stamp: 'FR 9/12/2018 16:54:29'!
<= unNumeroDePeano

	(unNumeroDePeano = I) ifTrue: [^false].
	(unNumeroDePeano = I) ifFalse: [^(self previous) <= (unNumeroDePeano previous)].! !

!II class methodsFor: 'as yet unclassified' stamp: 'FR 9/12/2018 18:16:23'!
descripcionDeErrorDeNoSePuedeDividirPorUnNumeroMayor

	^'No se puede dividir por un n�mero de Peano mayor'.! !

!II class methodsFor: 'as yet unclassified' stamp: 'FR 9/17/2018 13:57:25'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	previous := I.
	next := III.! !

!II class methodsFor: 'as yet unclassified' stamp: 'FR 9/12/2018 17:00:01'!
next

	 next ifNil:
	[
		next := self cloneNamed: self name, 'I'.
		next previous: self.
	].
	
	^next! !

!II class methodsFor: 'as yet unclassified' stamp: 'FR 9/12/2018 16:53:38'!
previous

	^previous! !

!II class methodsFor: 'as yet unclassified' stamp: 'FR 9/12/2018 16:54:12'!
previous: unNumeroDePeano

	previous _ unNumeroDePeano.! !

!II class methodsFor: 'as yet unclassified' stamp: 'FR 9/9/2018 21:23:37'!
removeAllNext
	
	next ifNotNil:
	[
		next removeAllNext.
		next removeFromSystem.
		next _ nil.
	].! !

!II class methodsFor: 'as yet unclassified' stamp: 'FR 9/12/2018 20:56:11'!
restaleA: unNumeroDePeano

	^(unNumeroDePeano previous) - (self previous)! !


!classDefinition: #III category: #'Tercera actividad'!
DenotativeObject subclass: #III
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Tercera actividad'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'III class' category: #'Tercera actividad'!
III class
	instanceVariableNames: 'previous next'!

!III class methodsFor: 'as yet unclassified' stamp: 'FR 9/17/2018 13:57:20'!
* unNumeroDePeano
	
	^ self previous * unNumeroDePeano + unNumeroDePeano! !

!III class methodsFor: 'as yet unclassified' stamp: 'FR 9/17/2018 13:57:20'!
+ unNumeroDePeano

	^ (self previous) + unNumeroDePeano next! !

!III class methodsFor: 'as yet unclassified' stamp: 'FR 9/17/2018 13:57:20'!
- unNumeroDePeano
	
	^unNumeroDePeano restaleA: self.! !

!III class methodsFor: 'as yet unclassified' stamp: 'FR 9/17/2018 13:57:20'!
/ unNumeroDePeano

	(unNumeroDePeano <= self) ifFalse: [self error: self descripcionDeErrorDeNoSePuedeDividirPorUnNumeroMayor].
	(self <= ((unNumeroDePeano + unNumeroDePeano) previous)) ifFalse: [^((self - unNumeroDePeano) / unNumeroDePeano) next].
	^I.
	! !

!III class methodsFor: 'as yet unclassified' stamp: 'FR 9/17/2018 13:57:20'!
<= unNumeroDePeano

	(unNumeroDePeano = I) ifTrue: [^false].
	(unNumeroDePeano = I) ifFalse: [^(self previous) <= (unNumeroDePeano previous)].! !

!III class methodsFor: 'as yet unclassified' stamp: 'FR 9/17/2018 13:57:20'!
descripcionDeErrorDeNoSePuedeDividirPorUnNumeroMayor

	^'No se puede dividir por un n�mero de Peano mayor'.! !

!III class methodsFor: 'as yet unclassified' stamp: 'FR 9/17/2018 13:57:25'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	previous := II.
	next := IIII.! !

!III class methodsFor: 'as yet unclassified' stamp: 'FR 9/17/2018 13:57:20'!
next

	 next ifNil:
	[
		next := self cloneNamed: self name, 'I'.
		next previous: self.
	].
	
	^next! !

!III class methodsFor: 'as yet unclassified' stamp: 'FR 9/17/2018 13:57:20'!
previous

	^previous! !

!III class methodsFor: 'as yet unclassified' stamp: 'FR 9/17/2018 13:57:20'!
previous: unNumeroDePeano

	previous _ unNumeroDePeano.! !

!III class methodsFor: 'as yet unclassified' stamp: 'FR 9/17/2018 13:57:20'!
removeAllNext
	
	next ifNotNil:
	[
		next removeAllNext.
		next removeFromSystem.
		next _ nil.
	].! !

!III class methodsFor: 'as yet unclassified' stamp: 'FR 9/17/2018 13:57:20'!
restaleA: unNumeroDePeano

	^(unNumeroDePeano previous) - (self previous)! !


!classDefinition: #IIII category: #'Tercera actividad'!
DenotativeObject subclass: #IIII
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Tercera actividad'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'IIII class' category: #'Tercera actividad'!
IIII class
	instanceVariableNames: 'previous next'!

!IIII class methodsFor: 'as yet unclassified' stamp: 'FR 9/17/2018 13:57:20'!
* unNumeroDePeano
	
	^ self previous * unNumeroDePeano + unNumeroDePeano! !

!IIII class methodsFor: 'as yet unclassified' stamp: 'FR 9/17/2018 13:57:20'!
+ unNumeroDePeano

	^ (self previous) + unNumeroDePeano next! !

!IIII class methodsFor: 'as yet unclassified' stamp: 'FR 9/17/2018 13:57:20'!
- unNumeroDePeano
	
	^unNumeroDePeano restaleA: self.! !

!IIII class methodsFor: 'as yet unclassified' stamp: 'FR 9/17/2018 13:57:20'!
/ unNumeroDePeano

	(unNumeroDePeano <= self) ifFalse: [self error: self descripcionDeErrorDeNoSePuedeDividirPorUnNumeroMayor].
	(self <= ((unNumeroDePeano + unNumeroDePeano) previous)) ifFalse: [^((self - unNumeroDePeano) / unNumeroDePeano) next].
	^I.
	! !

!IIII class methodsFor: 'as yet unclassified' stamp: 'FR 9/17/2018 13:57:20'!
<= unNumeroDePeano

	(unNumeroDePeano = I) ifTrue: [^false].
	(unNumeroDePeano = I) ifFalse: [^(self previous) <= (unNumeroDePeano previous)].! !

!IIII class methodsFor: 'as yet unclassified' stamp: 'FR 9/17/2018 13:57:20'!
descripcionDeErrorDeNoSePuedeDividirPorUnNumeroMayor

	^'No se puede dividir por un n�mero de Peano mayor'.! !

!IIII class methodsFor: 'as yet unclassified' stamp: 'FR 9/17/2018 13:57:25'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	previous := III.
	next := nil.! !

!IIII class methodsFor: 'as yet unclassified' stamp: 'FR 9/17/2018 13:57:20'!
next

	 next ifNil:
	[
		next := self cloneNamed: self name, 'I'.
		next previous: self.
	].
	
	^next! !

!IIII class methodsFor: 'as yet unclassified' stamp: 'FR 9/17/2018 13:57:20'!
previous

	^previous! !

!IIII class methodsFor: 'as yet unclassified' stamp: 'FR 9/17/2018 13:57:20'!
previous: unNumeroDePeano

	previous _ unNumeroDePeano.! !

!IIII class methodsFor: 'as yet unclassified' stamp: 'FR 9/17/2018 13:57:20'!
removeAllNext
	
	next ifNotNil:
	[
		next removeAllNext.
		next removeFromSystem.
		next _ nil.
	].! !

!IIII class methodsFor: 'as yet unclassified' stamp: 'FR 9/17/2018 13:57:20'!
restaleA: unNumeroDePeano

	^(unNumeroDePeano previous) - (self previous)! !

I initializeAfterFileIn!
II initializeAfterFileIn!
III initializeAfterFileIn!
IIII initializeAfterFileIn!