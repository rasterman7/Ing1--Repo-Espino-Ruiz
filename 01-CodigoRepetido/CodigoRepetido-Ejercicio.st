!classDefinition: #CantSuspend category: #'CodigoRepetido-Ejercicio'!
Error subclass: #CantSuspend
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!


!classDefinition: #NotFound category: #'CodigoRepetido-Ejercicio'!
Error subclass: #NotFound
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!


!classDefinition: #CustomerBookTest category: #'CodigoRepetido-Ejercicio'!
TestCase subclass: #CustomerBookTest
	instanceVariableNames: 'customerBook'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!

!CustomerBookTest methodsFor: 'testing' stamp: 'FR 9/23/2018 18:40:30'!
test01AddingCustomerShouldNotTakeMoreThan50Milliseconds
	
	self assert: [customerBook addCustomerNamed: 'John Lennon'] takesLessThan: 50.! !

!CustomerBookTest methodsFor: 'testing' stamp: 'FR 9/23/2018 18:41:56'!
test02RemovingCustomerShouldNotTakeMoreThan100Milliseconds

	| paulMcCartney |
	
	paulMcCartney := 'Paul McCartney'.
	
	customerBook addCustomerNamed: paulMcCartney.
	
	self assert: [customerBook removeCustomerNamed: paulMcCartney] takesLessThan: 100.
	
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'FR 9/23/2018 18:42:09'!
test03CanNotAddACustomerWithEmptyName 

	self should: [customerBook addCustomerNamed: ''.]
		return: Error
		evaluate: [ :anError | 
			self assert: anError messageText = CustomerBook customerCanNotBeEmptyErrorMessage.
			self assert: customerBook isEmpty ]. ! !

!CustomerBookTest methodsFor: 'testing' stamp: 'FR 9/23/2018 18:42:35'!
test04CanNotRemoveAnInvalidCustomer
	
	| johnLennon |
	
	johnLennon := 'John Lennon'.
	customerBook addCustomerNamed: johnLennon.
	
	self should: [customerBook removeCustomerNamed: 'Paul McCartney']
		return: NotFound
		evaluate: [ :anError | 
			self assert: customerBook numberOfCustomers = 1.
			self assert: (customerBook includesCustomerNamed: johnLennon) ].! !

!CustomerBookTest methodsFor: 'testing' stamp: 'FR 9/23/2018 18:42:50'!
test05SuspendingACustomerShouldNotRemoveItFromCustomerBook

	| paulMcCartney |
	
	paulMcCartney := 'Paul McCartney'.
	
	customerBook addCustomerNamed: paulMcCartney.
	customerBook suspendCustomerNamed: paulMcCartney.
	
	self assert: customerBook hasActive: 0 andSuspended: 1 among: 1.
	self assert: (customerBook includesCustomerNamed: paulMcCartney).
	

	
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'FR 9/23/2018 18:43:59'!
test06RemovingASuspendedCustomerShouldRemoveItFromCustomerBook

	| paulMcCartney |
	
	paulMcCartney := 'Paul McCartney'.
	
	customerBook addCustomerNamed: paulMcCartney.
	customerBook suspendCustomerNamed: paulMcCartney.
	customerBook removeCustomerNamed: paulMcCartney.
	
	self assert: customerBook hasActive: 0 andSuspended: 0 among: 0.
	self deny: (customerBook includesCustomerNamed: paulMcCartney).


	
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'FR 9/23/2018 18:44:11'!
test07CanNotSuspendAnInvalidCustomer
	
	| johnLennon |
	
	johnLennon := 'John Lennon'.
	customerBook addCustomerNamed: johnLennon.
	
	self assert: customerBook includesJohnLennonAndCantSuspend: 'Paul McCartney'
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'FR 9/23/2018 18:44:20'!
test08CanNotSuspendAnAlreadySuspendedCustomer
	
	| johnLennon |
	
	johnLennon := 'John Lennon'.
	customerBook addCustomerNamed: johnLennon.
	customerBook suspendCustomerNamed: johnLennon.
	
	self assert: customerBook includesJohnLennonAndCantSuspend: johnLennon! !


!CustomerBookTest methodsFor: 'assertions' stamp: 'FR 9/21/2018 17:14:30'!
assert: customerBook hasActive: aNumberOfActiveCustomers andSuspended: aNumberOfSuspendedCustomers among: aNumberOfCustomers
	
	self assert: aNumberOfActiveCustomers equals: customerBook numberOfActiveCustomers.
	self assert: aNumberOfSuspendedCustomers equals: customerBook numberOfSuspendedCustomers.
	self assert: aNumberOfCustomers equals: customerBook numberOfCustomers.! !

!CustomerBookTest methodsFor: 'assertions' stamp: 'FR 9/21/2018 17:39:25'!
assert: customerBook includesJohnLennonAndCantSuspend: aNonActiveCustomer
	
	[ customerBook suspendCustomerNamed: aNonActiveCustomer.
	self fail ]
		on: CantSuspend 
		do: [ :anError | 
			self assert: customerBook numberOfCustomers = 1.
			self assert: (customerBook includesCustomerNamed: 'John Lennon') ]
! !

!CustomerBookTest methodsFor: 'assertions' stamp: 'IE 9/23/2018 23:55:28'!
assert: aClosure takesLessThan: aLimitNumberOfMilliseconds
	| millisecondsBeforeRunning millisecondsAfterRunning |
	  
	millisecondsBeforeRunning := Time millisecondClockValue * millisecond.
	aClosure value.
	millisecondsAfterRunning := Time millisecondClockValue * millisecond.
	
	self assert: (millisecondsAfterRunning-millisecondsBeforeRunning) < (aLimitNumberOfMilliseconds * millisecond)! !


!CustomerBookTest methodsFor: 'exception' stamp: 'FR 9/23/2018 18:39:49'!
setUp
	
	customerBook _ CustomerBook new! !

!CustomerBookTest methodsFor: 'exception' stamp: 'FR 9/23/2018 15:36:00'!
should: aClosure return: anExceptionSignal evaluate: anExceptionHandler

	[ aClosure value.
	self fail ]
		on: anExceptionSignal
		do: anExceptionSignal.! !


!classDefinition: #CustomerBook category: #'CodigoRepetido-Ejercicio'!
Object subclass: #CustomerBook
	instanceVariableNames: 'suspended active'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!

!CustomerBook methodsFor: 'testing' stamp: 'FR 9/23/2018 15:30:14'!
includesActiveCustomerNamed: aName

	^active includes: aName! !

!CustomerBook methodsFor: 'testing' stamp: 'FR 9/21/2018 16:18:24'!
includesCustomerNamed: aName

	^(self includesActiveCustomerNamed: aName) or: [ suspended includes: aName ]! !

!CustomerBook methodsFor: 'testing' stamp: 'FR 9/23/2018 15:30:14'!
isEmpty
	
	^active isEmpty and: [ suspended isEmpty ]! !


!CustomerBook methodsFor: 'initialization' stamp: 'FR 9/23/2018 15:30:14'!
initialize

	super initialize.
	active := OrderedCollection new.
	suspended:= OrderedCollection new.! !


!CustomerBook methodsFor: 'customer management' stamp: 'FR 9/23/2018 15:30:14'!
addCustomerNamed: aName

	aName isEmpty ifTrue: [ self signalCustomerNameCannotBeEmpty ].
	(self includesCustomerNamed: aName) ifTrue: [ self signalCustomerAlreadyExists ].
	
	active add: aName ! !

!CustomerBook methodsFor: 'customer management' stamp: 'FR 9/23/2018 15:30:14'!
numberOfActiveCustomers
	
	^active size! !

!CustomerBook methodsFor: 'customer management' stamp: 'FR 9/21/2018 14:50:48'!
numberOfCustomers
	
	^self numberOfActiveCustomers + self numberOfSuspendedCustomers ! !

!CustomerBook methodsFor: 'customer management' stamp: 'NR 9/19/2018 17:36:09'!
numberOfSuspendedCustomers
	
	^suspended size! !

!CustomerBook methodsFor: 'customer management' stamp: 'FR 9/23/2018 18:34:27'!
removeCustomerNamed: aName

	^active remove: aName ifAbsent: [
		suspended remove: aName ifAbsent: [ NotFound signal ]
	].
! !

!CustomerBook methodsFor: 'customer management' stamp: 'HernanWilkinson 7/6/2011 17:52'!
signalCustomerAlreadyExists 

	self error: self class customerAlreadyExistsErrorMessage! !

!CustomerBook methodsFor: 'customer management' stamp: 'HernanWilkinson 7/6/2011 17:51'!
signalCustomerNameCannotBeEmpty 

	self error: self class customerCanNotBeEmptyErrorMessage ! !

!CustomerBook methodsFor: 'customer management' stamp: 'FR 9/23/2018 15:30:14'!
suspendCustomerNamed: aName 
	
	(self includesActiveCustomerNamed: aName) ifFalse: [^CantSuspend signal].
	
	active remove: aName.
	
	suspended add: aName! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CustomerBook class' category: #'CodigoRepetido-Ejercicio'!
CustomerBook class
	instanceVariableNames: ''!

!CustomerBook class methodsFor: 'error messages' stamp: 'HernanWilkinson 7/6/2011 17:57'!
customerAlreadyExistsErrorMessage

	^'Customer already exists'! !

!CustomerBook class methodsFor: 'error messages' stamp: 'HernanWilkinson 7/6/2011 17:53'!
customerCanNotBeEmptyErrorMessage

	^'Customer name cannot be empty'! !


